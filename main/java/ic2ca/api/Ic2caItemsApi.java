package ic2ca.api;

import cpw.mods.fml.common.Loader;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class Ic2caItemsApi
{
	private static Class items;
	private static boolean isLoaded = false;

	public static ItemStack getItemByName(String name)
	{
		try
		{
			if(items == null && !isLoaded)
			{
				isLoaded = Loader.isModLoaded("IC2CA");
				items = Class.forName("ic2ca.common.Ic2caItems");
			}

			Object item = items.getField(name).get(null);

			if(item instanceof ItemStack)
			{
				return (ItemStack)item;
			}
			else if(item instanceof Item)
			{
				return new ItemStack((Item)item, 1, 0);
			}
			else if(item instanceof Block)
			{
				return new ItemStack((Block)item, 1, 0);
			}
			else
			{
				System.out.println("IC2CA Api failed to find an item with name '" + name + "'. See Ic2caItemsApi.java for information of valid names.");
				return null;
			}
		}
		catch(Exception e)
		{
			System.out.println("IC2CA Api failed to load item for name '" + name + "'. Stack trace: ");
			e.printStackTrace();
			return null;
		}
	}
}
