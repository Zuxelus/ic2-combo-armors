package ic2ca.client;

import ic2.core.util.StackUtil;
import ic2ca.common.IC2CA;
import ic2ca.common.item.armor.ItemArmorBaseJetpack;

import java.util.EnumSet;

import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;

public class ClientTickHandleric2ca implements ITickHandler
{
	public static Minecraft mc = FMLClientHandler.instance().getClient();
	public static boolean isFlyActiveByMod = false;
	public static boolean isCloakActiveByMod = false;
	public static boolean isFirstLoad = false;
	public static boolean isLastFlyUndressed = false;
	public static boolean isLastCloakUndressed = false;
	public static boolean isLastCreativeState = false;
	public static NBTTagCompound helmetNBT = new NBTTagCompound();
	public static NBTTagCompound pantsNBT = new NBTTagCompound();
	public static NBTTagCompound bootsNBT = new NBTTagCompound();
	public static NBTTagCompound chestNBT = new NBTTagCompound();

	public String getLabel()
	{
		return "ic2ca";
	}

	@SuppressWarnings({ "rawtypes" })
	public void tickEnd(EnumSet<TickType> type, Object... tickData) {}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public EnumSet ticks()
	{
		return EnumSet.of(TickType.WORLD, TickType.WORLDLOAD, TickType.CLIENT, TickType.RENDER);
	}

	@SuppressWarnings("rawtypes")  
	public void tickStart(EnumSet<TickType> type, Object... tickData)
	{
		if (type.contains(TickType.CLIENT) && mc.theWorld != null)
		{
			if (!isFirstLoad)
			{
				isFirstLoad = true;
				for (int i = 0; i < mc.gameSettings.keyBindings.length; i++)
				{
					if (mc.gameSettings.keyBindings[i].keyDescription == "Boost Key") 
					{
						KeyboardClientic2ca.icBoostKeyID = i;
					}
				}
			}
			ItemStack armor = mc.thePlayer.inventory.armorInventory[2];
			if (armor != null && (armor.getItem() instanceof ItemArmorBaseJetpack))
			{
				NBTTagCompound nbt = StackUtil.getOrCreateNbtData(armor);
				if (nbt.getBoolean("flight"))
				{
					if (mc.thePlayer.capabilities.isCreativeMode && !isLastCreativeState)
					{
						isLastCreativeState = true;
					}
					else if (!mc.thePlayer.capabilities.isCreativeMode && isLastCreativeState && isFlyActiveByMod)
					{
						mc.thePlayer.capabilities.allowFlying = true;
						mc.thePlayer.capabilities.isFlying = true;
						isLastCreativeState = false;
					}
					UpgradesClientProxy.onFlyTickClient(mc.thePlayer, armor/*, KeyboardClientic2ca.moveStrafe, KeyboardClientic2ca.moveForward*/);
					if (mc.thePlayer.posY > 262.0D && !mc.thePlayer.capabilities.isCreativeMode)
					{
						mc.thePlayer.setPosition(mc.thePlayer.posX, 262.0D, mc.thePlayer.posZ);
					}
				}
				else if (isFlyActiveByMod)
				{
					mc.thePlayer.capabilities.allowFlying = false;
					mc.thePlayer.capabilities.isFlying = false;
					isFlyActiveByMod = false;
					isLastFlyUndressed = true;
				}
			}
			else if (isFlyActiveByMod)
			{
				mc.thePlayer.capabilities.allowFlying = false;
				mc.thePlayer.capabilities.isFlying = false;
				isFlyActiveByMod = false;
				isLastFlyUndressed = true;
			}
			NBTTagCompound chestnbt = chestNBT;
			if (armor != null && IC2CA.chests.contains(armor.getItem().itemID))
			{
				NBTTagCompound nbt = StackUtil.getOrCreateNbtData(armor);
				if (nbt.getBoolean("cloaking"))
				{
					if (isCloakActiveByMod) 
					{
						mc.thePlayer.addPotionEffect(new PotionEffect(Potion.invisibility.id, 19, 1));
					}
					UpgradesClientProxy.onCloakTickClient(mc.thePlayer, armor, KeyboardClientic2ca.moveStrafe, KeyboardClientic2ca.moveForward);
				}
			}
			else if (chestnbt != null && chestnbt.getBoolean("has"))
			{
				int id = chestnbt.getInteger("id");
				int damage = chestnbt.getInteger("damage");
				int size = chestnbt.getInteger("size");
				ItemStack is = new ItemStack(id, size, damage);
				if (chestnbt.getCompoundTag("nbt") != null)
				{
					NBTTagCompound nbt = chestnbt.getCompoundTag("nbt");
					is.setTagCompound(nbt);
				}
				if (is != null && IC2CA.chests.contains(is.getItem().itemID))
				{
					NBTTagCompound nbt = StackUtil.getOrCreateNbtData(is);
					if (nbt.getBoolean("cloaking"))
					{
						if (isCloakActiveByMod) 
						{
							mc.thePlayer.addPotionEffect(new PotionEffect(Potion.invisibility.id, 19, 1));
						}
						UpgradesClientProxy.onCloakTickClient(mc.thePlayer, is, KeyboardClientic2ca.moveStrafe, KeyboardClientic2ca.moveForward);
					}
				}
				else if (isCloakActiveByMod)
				{
					mc.thePlayer.removePotionEffect(Potion.invisibility.id);
					isCloakActiveByMod = false;
					isLastCloakUndressed = true;
				}
			}
		}
	}
}
