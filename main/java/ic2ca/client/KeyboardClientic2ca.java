package ic2ca.client;

import ic2.core.IC2;
import ic2.core.util.StackUtil;
import ic2ca.common.IC2CA;
import ic2ca.common.item.armor.IJetpack;
import ic2ca.common.item.armor.ItemArmorBaseJetpack;

import java.util.EnumSet;

import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.client.registry.KeyBindingRegistry.KeyHandler;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class KeyboardClientic2ca extends KeyHandler
{
	public static Minecraft mc = FMLClientHandler.instance().getClient();
	public static KeyBinding flyKey = new KeyBinding("IC2CA Upgrades Key", Keyboard.KEY_GRAVE);
	private static int lastKeyState = 0;
	public static int icBoostKeyID;
	public static float moveStrafe;
	public static float moveForward;

	public static boolean isBoostKeyDown(EntityPlayer player)
	{
		return mc.gameSettings.keyBindings[icBoostKeyID].pressed;
	}

	public static boolean isJumpKeyDown(EntityPlayer player)
	{
		return mc.gameSettings.keyBindJump.pressed;
	}

	public static boolean isSneakKeyDown(EntityPlayer player)
	{
		return mc.gameSettings.keyBindSneak.pressed;
	}

	public static void sendKeyUpdate(EntityPlayer player)
	{
		int state = (isBoostKeyDown(player) ? 1 : 0) << 0 | (isJumpKeyDown(player) ? 1 : 0) << 1 | (isSneakKeyDown(player) ? 1 : 0) << 2;
		if (state != lastKeyState)
		{
			ClientProxyic2ca.sendMyPacket("keyState", state, mc.thePlayer);
			lastKeyState = state;
		}
	}

	public static void updatePlayerMove()
	{
		moveStrafe = 0.0F;
		moveForward = 0.0F;
		if (mc.gameSettings.keyBindForward.pressed) {
			++moveForward;
		}
		if (mc.gameSettings.keyBindBack.pressed) {
			--moveForward;
		}
		if (mc.gameSettings.keyBindLeft.pressed) {
			++moveStrafe;
		}
		if (mc.gameSettings.keyBindRight.pressed) {
			--moveStrafe;
		}
		if (mc.gameSettings.keyBindSneak.pressed)
		{
			moveStrafe = (float)((double)moveStrafe * 0.3D);
			moveForward = (float)((double)moveForward * 0.3D);
		}
	}

	public KeyboardClientic2ca()
	{
		super(new KeyBinding[] { flyKey }, new boolean[] { false });
	}

	public String getLabel()
	{
		return null;
	}

	@SuppressWarnings({ "rawtypes" })
	public void keyDown(EnumSet types, KeyBinding kb, boolean tickEnd, boolean isRepeat) {}

	@SuppressWarnings({ "rawtypes" })
	public void keyUp(EnumSet types, KeyBinding kb, boolean tickEnd)
	{
		if (tickEnd && kb == flyKey && mc.inGameHasFocus)
		{
			ItemStack armor = mc.thePlayer.inventory.armorInventory[2];
            //Flight toggle
			if (!IC2.keyboard.isModeSwitchKeyDown(mc.thePlayer) && !mc.gameSettings.keyBindAttack.pressed)
			{
				if (armor != null && armor.getItem() instanceof ItemArmorBaseJetpack)
				{
					NBTTagCompound nbt = StackUtil.getOrCreateNbtData(armor);
					if (nbt.getBoolean("flight"))
					{
						UpgradesClientProxy.switchFlyModeClient(mc.thePlayer, armor);
					}
					else
					{
						IJetpack ij = (IJetpack)armor.getItem();
						ij.onFlykeyPressed(armor, mc.thePlayer);
					}
					ClientProxyic2ca.sendMyPacket("flyToggle", 1, mc.thePlayer);
				}
			}
            //Overcharge
			if (!IC2.keyboard.isModeSwitchKeyDown(mc.thePlayer) && mc.gameSettings.keyBindAttack.pressed) 
			{
				if (armor != null && IC2CA.chests.contains(armor.getItem().itemID))
				{
					NBTTagCompound nbt = StackUtil.getOrCreateNbtData(armor);
					if (nbt.getBoolean("overcharge"))
					{
						ClientProxyic2ca.sendMyPacket("overcharge", 1, mc.thePlayer);
						UpgradesClientProxy.overcharge(mc.thePlayer, armor);
					}
				}
			}
            
            //Cloak toggle
			if (IC2.keyboard.isModeSwitchKeyDown(mc.thePlayer) && !mc.gameSettings.keyBindAttack.pressed)
			{
				NBTTagCompound chestnbt = ClientTickHandleric2ca.chestNBT;
				if (armor != null && IC2CA.chests.contains(armor.getItem().itemID))
				{
					NBTTagCompound nbt = StackUtil.getOrCreateNbtData(armor);
					if (nbt.getBoolean("cloaking"))
					{
						ClientProxyic2ca.sendMyPacket("cloakToggle", 1, mc.thePlayer);
						UpgradesClientProxy.switchCloakModeClient(mc.thePlayer, armor);
					}
				}
				else if (chestnbt != null && chestnbt.getBoolean("has"))
				{
					int id = chestnbt.getInteger("id");
					int damage = chestnbt.getInteger("damage");
					int size = chestnbt.getInteger("size");
					ItemStack is = new ItemStack(id, size, damage);
					if (chestnbt.getCompoundTag("nbt") != null)
					{
						NBTTagCompound nbt = chestnbt.getCompoundTag("nbt");
						is.setTagCompound(nbt);
					}
					if (is != null && IC2CA.chests.contains(is.getItem().itemID))
					{
						NBTTagCompound nbt = StackUtil.getOrCreateNbtData(is);
						if (nbt.getBoolean("cloaking"))
						{
							ClientProxyic2ca.sendMyPacket("cloakToggle", 1, mc.thePlayer);
							UpgradesClientProxy.switchCloakModeClient(mc.thePlayer, is);
						}
					}
				}
			}
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public EnumSet ticks()
	{
		return EnumSet.of(TickType.CLIENT);
	}
}
