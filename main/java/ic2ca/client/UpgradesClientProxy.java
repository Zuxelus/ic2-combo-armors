package ic2ca.client;

import ic2.api.item.ElectricItem;
import ic2.core.util.StackUtil;
import ic2ca.common.IC2CA;
import ic2ca.common.item.ItemNBTHelper;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;

public class UpgradesClientProxy
{
	public static boolean firstLoad = false;

	public static boolean firstLoadClient(EntityPlayer player, ItemStack stack)
	{
		if (ItemNBTHelper.readFlyStatus(stack))
		{
			ItemNBTHelper.saveFlyStatus(stack, false);
			switchFlyModeClient(player, stack);
		}
		if (ItemNBTHelper.readCloakStatus(stack))
		{
			ItemNBTHelper.saveCloakStatus(stack, false);
			switchCloakModeClient(player, stack);
		}
		return true;
	}

	public static boolean onCloakTickClient(EntityPlayer player, ItemStack stack, float var2, float var3)
	{
		if (firstLoad)
		{
			ClientProxyic2ca.sendMyPacket("worldLoad", 1, player);
			firstLoad = false;
			return true;
		}
		if (ClientTickHandleric2ca.isLastCloakUndressed)
		{
			ItemNBTHelper.saveCloakStatus(stack, false);
			ClientTickHandleric2ca.isLastCloakUndressed = false;
		}
		if (ItemNBTHelper.readCloakStatus(stack))
		{
			int var4 = ItemNBTHelper.getCharge(stack);
			if (player.isDead && (!player.worldObj.getGameRules().getGameRuleBooleanValue("keepInventory")))
			{
				NBTTagCompound helmetnbt = ClientTickHandleric2ca.helmetNBT;
				if ((helmetnbt != null) && helmetnbt.getBoolean("has"))
				{
					int id = helmetnbt.getInteger("id");
					int damage = helmetnbt.getInteger("damage");
					int size = helmetnbt.getInteger("size");
					ItemStack is = new ItemStack(id, size, damage);
					if (helmetnbt.getCompoundTag("nbt") != null)
					{
						NBTTagCompound nbt = helmetnbt.getCompoundTag("nbt");
						is.setTagCompound(nbt);
					}
					EntityItem entityitem = new EntityItem(player.worldObj, (double)player.posX + 0.5D, (double)player.posY + 0.5D, (double)player.posZ + 0.5D, is);
					entityitem.delayBeforeCanPickup = 10;
					if (!player.worldObj.isRemote) 
					{
						player.worldObj.spawnEntityInWorld(entityitem);
					}
					helmetnbt.removeTag("id");
					helmetnbt.removeTag("damage");
					helmetnbt.removeTag("size");
					helmetnbt.removeTag("nbt");
					helmetnbt.removeTag("has");
				}
				NBTTagCompound pantsnbt = ClientTickHandleric2ca.pantsNBT;
				if ((pantsnbt != null) && (pantsnbt.getBoolean("has")))
				{
					int id = pantsnbt.getInteger("id");
					int damage = pantsnbt.getInteger("damage");
					int size = pantsnbt.getInteger("size");
					ItemStack is = new ItemStack(id, size, damage);
					if (pantsnbt.getCompoundTag("nbt") != null)
					{
						NBTTagCompound nbt = pantsnbt.getCompoundTag("nbt");
						is.setTagCompound(nbt);
					}
					EntityItem entityitem = new EntityItem(player.worldObj, (double)player.posX + 0.5D, (double)player.posY + 0.5D, (double)player.posZ + 0.5D, is);
					entityitem.delayBeforeCanPickup = 10;
					if (!player.worldObj.isRemote) {
						player.worldObj.spawnEntityInWorld(entityitem);
					}
					pantsnbt.removeTag("id");
					pantsnbt.removeTag("damage");
					pantsnbt.removeTag("size");
					pantsnbt.removeTag("nbt");
					pantsnbt.removeTag("has");
				}
				NBTTagCompound bootsnbt = ClientTickHandleric2ca.bootsNBT;
				if ((bootsnbt != null) && (bootsnbt.getBoolean("has")))
				{
					int id = bootsnbt.getInteger("id");
					int damage = bootsnbt.getInteger("damage");
					int size = bootsnbt.getInteger("size");
					ItemStack is = new ItemStack(id, size, damage);
					if (bootsnbt.getCompoundTag("nbt") != null)
					{
						NBTTagCompound nbt = bootsnbt.getCompoundTag("nbt");
						is.setTagCompound(nbt);
					}
					EntityItem entityitem = new EntityItem(player.worldObj, (double)player.posX + 0.5D, (double)player.posY + 0.5D, (double)player.posZ + 0.5D, is);
					entityitem.delayBeforeCanPickup = 10;
					if (!player.worldObj.isRemote) {
						player.worldObj.spawnEntityInWorld(entityitem);
					}
					bootsnbt.removeTag("id");
					bootsnbt.removeTag("damage");
					bootsnbt.removeTag("size");
					bootsnbt.removeTag("nbt");
					bootsnbt.removeTag("has");
				}
				NBTTagCompound chestnbt = ClientTickHandleric2ca.chestNBT;
				if ((chestnbt != null) && (chestnbt.getBoolean("has")))
				{
					int id = chestnbt.getInteger("id");
					int damage = chestnbt.getInteger("damage");
					int size = chestnbt.getInteger("size");
					ItemStack is = new ItemStack(id, size, damage);
					if (chestnbt.getCompoundTag("nbt") != null)
					{
						NBTTagCompound nbt = chestnbt.getCompoundTag("nbt");
						is.setTagCompound(nbt);
					}
					EntityItem entityitem = new EntityItem(player.worldObj, (double)player.posX + 0.5D, (double)player.posY + 0.5D, (double)player.posZ + 0.5D, is);
					entityitem.delayBeforeCanPickup = 10;
					if (!player.worldObj.isRemote) {
						player.worldObj.spawnEntityInWorld(entityitem);
					}
					switchCloakModeClient(player, is);
					chestnbt.removeTag("id");
					chestnbt.removeTag("damage");
					chestnbt.removeTag("size");
					chestnbt.removeTag("nbt");
					chestnbt.removeTag("has");
				}
			}
			else if ((player.isDead) && (player.worldObj.getGameRules().getGameRuleBooleanValue("keepInventory")))
			{
				NBTTagCompound chestnbt = ClientTickHandleric2ca.chestNBT;
				if ((chestnbt != null) && (chestnbt.getBoolean("has")))
				{
					int id = chestnbt.getInteger("id");
					int damage = chestnbt.getInteger("damage");
					int size = chestnbt.getInteger("size");
					ItemStack is = new ItemStack(id, size, damage);
					if (chestnbt.getCompoundTag("nbt") != null)
					{
						NBTTagCompound nbt = chestnbt.getCompoundTag("nbt");
						is.setTagCompound(nbt);
					}
					switchCloakModeClient(player, is);
				}
			}
			if (!player.capabilities.isCreativeMode) {
				if (var4 < 10)
				{
					ClientProxyic2ca.sendPlayerMessage(player, "Out of energy!");
					switchCloakModeClient(player, stack);
				}
				else
				{
					ElectricItem.manager.discharge(stack, 10, 4, true, false);
				}
			}
			KeyboardClientic2ca.sendKeyUpdate(player);
		}
		return true;
	}

	public static boolean onFlyTickClient(EntityPlayer player, ItemStack stack/*, float var2, float var3*/)
	{
		if (firstLoad)
		{
			ClientProxyic2ca.sendMyPacket("worldLoad", 1, player);
			firstLoad = false;
			return true;
		}
		if (ClientTickHandleric2ca.isLastFlyUndressed)
		{
			ItemNBTHelper.saveFlyStatus(stack, false);
			ClientTickHandleric2ca.isLastFlyUndressed = false;
		}
		if (ItemNBTHelper.readFlyStatus(stack))
		{
			int var4 = ItemNBTHelper.getCharge(stack);
			if (!player.capabilities.isCreativeMode) {
				if (var4 < 10)
				{
					ClientProxyic2ca.sendPlayerMessage(player, "Out of energy!");
					switchFlyModeClient(player, stack);
				}
				else if (player.capabilities.isFlying)
				{
					ElectricItem.manager.discharge(stack, IC2CA.turbineEUAmount, 4, true, false);
				}
			}
			KeyboardClientic2ca.sendKeyUpdate(player);
			player.fallDistance = 0.0F;
		}
		return true;
	}

	public static void overcharge(EntityPlayer player, ItemStack stack)
	{
		int charge = ItemNBTHelper.getCharge(stack);
		int maxcharge = StackUtil.getOrCreateNbtData(stack).getInteger("maxCharge");
		int overchargenumber = maxcharge / 10;
		int boltnumber = overchargenumber / 10000;
		if (boltnumber < 1)
		{
			ClientProxyic2ca.sendPlayerMessage(player, "This " + stack.getDisplayName() + " does not have a high enough max charge to discharge.");
		}
		else
		{
			if (boltnumber >= 10) {
				boltnumber = 10;
			}
			if (charge >= boltnumber * 10000)
			{
				MovingObjectPosition mop = player.rayTrace(75.0D, 1.0F);
				if (mop != null)
				{
					Vec3 vec3 = mop.hitVec;
					double x = vec3.xCoord;
					double y = vec3.yCoord;
					double z = vec3.zCoord;
					for (int q = 1; q <= boltnumber; q++)
					{
						EntityLightningBolt elb = new EntityLightningBolt(ClientProxyic2ca.mc.theWorld, x, y, z);
						ClientProxyic2ca.mc.theWorld.spawnEntityInWorld(elb);
						ElectricItem.manager.discharge(stack, 10000, 4, true, false);
					}
					ClientProxyic2ca.sendPlayerMessage(player, "Discharged " + boltnumber * 10000 + " EU.");
				}
			}
			else
			{
				ClientProxyic2ca.sendPlayerMessage(player, "Not enough energy to discharge!");
			}
		}
	}

	public static boolean switchCloakModeClient(EntityPlayer player, ItemStack stack)
	{
		if (ItemNBTHelper.readCloakStatus(stack))
		{
			player.removePotionEffect(Potion.invisibility.id);

			NBTTagCompound helmetnbt = ClientTickHandleric2ca.helmetNBT;
			if ((helmetnbt != null) && (helmetnbt.getBoolean("has")))
			{
				int id = helmetnbt.getInteger("id");
				int damage = helmetnbt.getInteger("damage");
				int size = helmetnbt.getInteger("size");
				ItemStack is = new ItemStack(id, size, damage);
				if (helmetnbt.getCompoundTag("nbt") != null)
				{
					NBTTagCompound nbt = helmetnbt.getCompoundTag("nbt");
					is.setTagCompound(nbt);
				}
				player.inventory.armorInventory[3] = is;
				helmetnbt.removeTag("id");
				helmetnbt.removeTag("damage");
				helmetnbt.removeTag("size");
				helmetnbt.removeTag("nbt");
				helmetnbt.removeTag("has");
			}
			NBTTagCompound chestnbt = ClientTickHandleric2ca.chestNBT;
			if ((chestnbt != null) && (chestnbt.getBoolean("has")))
			{
				int id = chestnbt.getInteger("id");
				int damage = chestnbt.getInteger("damage");
				int size = chestnbt.getInteger("size");
				ItemStack is = new ItemStack(id, size, damage);
				if (chestnbt.getCompoundTag("nbt") != null)
				{
					NBTTagCompound nbt = chestnbt.getCompoundTag("nbt");
					is.setTagCompound(nbt);
				}
				player.inventory.armorInventory[2] = is;
				chestnbt.removeTag("id");
				chestnbt.removeTag("damage");
				chestnbt.removeTag("size");
				chestnbt.removeTag("nbt");
				chestnbt.removeTag("has");
			}
			NBTTagCompound pantsnbt = ClientTickHandleric2ca.pantsNBT;
			if ((pantsnbt != null) && pantsnbt.getBoolean("has"))
			{
				int id = pantsnbt.getInteger("id");
				int damage = pantsnbt.getInteger("damage");
				int size = pantsnbt.getInteger("size");
				ItemStack is = new ItemStack(id, size, damage);
				if (pantsnbt.getCompoundTag("nbt") != null)
				{
					NBTTagCompound nbt = pantsnbt.getCompoundTag("nbt");
					is.setTagCompound(nbt);
				}
				player.inventory.armorInventory[1] = is;
				pantsnbt.removeTag("id");
				pantsnbt.removeTag("damage");
				pantsnbt.removeTag("size");
				pantsnbt.removeTag("nbt");
				pantsnbt.removeTag("has");
			}
			NBTTagCompound bootsnbt = ClientTickHandleric2ca.bootsNBT;
			if ((bootsnbt != null) && (bootsnbt.getBoolean("has")))
			{
				int id = bootsnbt.getInteger("id");
				int damage = bootsnbt.getInteger("damage");
				int size = bootsnbt.getInteger("size");
				ItemStack is = new ItemStack(id, size, damage);
				if (bootsnbt.getCompoundTag("nbt") != null)
				{
					NBTTagCompound nbt = bootsnbt.getCompoundTag("nbt");
					is.setTagCompound(nbt);
				}
				player.inventory.armorInventory[0] = is;
				bootsnbt.removeTag("id");
				bootsnbt.removeTag("damage");
				bootsnbt.removeTag("size");
				bootsnbt.removeTag("nbt");
				bootsnbt.removeTag("has");
			}
			ClientProxyic2ca.sendPlayerMessage(player, "Cloaking engine disabled.");
			ClientTickHandleric2ca.isCloakActiveByMod = false;
			ItemNBTHelper.saveCloakStatus(stack, false);
		}
		else
		{
			int var2 = ItemNBTHelper.getCharge(stack);
			if ((var2 < 10) && (!player.capabilities.isCreativeMode))
			{
				ClientProxyic2ca.sendPlayerMessage(player, "Not enough energy to enable cloaking engine!");
			}
			else
			{
				ClientProxyic2ca.sendPlayerMessage(player, "Cloaking engine enabled.");
				player.addPotionEffect(new PotionEffect(Potion.invisibility.id, 19, 1));

				NBTTagCompound helmetnbt = ClientTickHandleric2ca.helmetNBT;
				if (player.inventory.armorInventory[3] != null)
				{
					helmetnbt.setBoolean("has", true);
					helmetnbt.setInteger("id", player.inventory.armorInventory[3].getItem().itemID);
					helmetnbt.setInteger("damage", player.inventory.armorInventory[3].getItemDamage());
					helmetnbt.setInteger("size", player.inventory.armorInventory[3].stackSize);
					if (player.inventory.armorInventory[3].getTagCompound() != null) {
						helmetnbt.setCompoundTag("nbt", StackUtil.getOrCreateNbtData(player.inventory.armorInventory[3]));
					}
					player.inventory.armorInventory[3] = null;
					ClientTickHandleric2ca.helmetNBT = helmetnbt;
				}
				NBTTagCompound chestnbt = ClientTickHandleric2ca.chestNBT;
				if (player.inventory.armorInventory[2] != null)
				{
					chestnbt.setBoolean("has", true);
					chestnbt.setInteger("id", player.inventory.armorInventory[2].getItem().itemID);
					chestnbt.setInteger("damage", player.inventory.armorInventory[2].getItemDamage());
					chestnbt.setInteger("size", player.inventory.armorInventory[2].stackSize);
					if (player.inventory.armorInventory[2].getTagCompound() != null) {
						chestnbt.setCompoundTag("nbt", StackUtil.getOrCreateNbtData(player.inventory.armorInventory[2]));
					}
					player.inventory.armorInventory[2] = null;
					ClientTickHandleric2ca.chestNBT = chestnbt;
				}
				NBTTagCompound pantsnbt = ClientTickHandleric2ca.pantsNBT;
				if (player.inventory.armorInventory[1] != null)
				{
					pantsnbt.setBoolean("has", true);
					pantsnbt.setInteger("id", player.inventory.armorInventory[1].getItem().itemID);
					pantsnbt.setInteger("damage", player.inventory.armorInventory[1].getItemDamage());
					pantsnbt.setInteger("size", player.inventory.armorInventory[1].stackSize);
					if (player.inventory.armorInventory[1].getTagCompound() != null) {
						pantsnbt.setCompoundTag("nbt", StackUtil.getOrCreateNbtData(player.inventory.armorInventory[1]));
					}
					player.inventory.armorInventory[1] = null;
					ClientTickHandleric2ca.pantsNBT = pantsnbt;
				}
				NBTTagCompound bootsnbt = ClientTickHandleric2ca.bootsNBT;
				if (player.inventory.armorInventory[0] != null)
				{
					bootsnbt.setBoolean("has", true);
					bootsnbt.setInteger("id", player.inventory.armorInventory[0].getItem().itemID);
					bootsnbt.setInteger("damage", player.inventory.armorInventory[0].getItemDamage());
					bootsnbt.setInteger("size", player.inventory.armorInventory[0].stackSize);
					if (player.inventory.armorInventory[0].getTagCompound() != null) {
						bootsnbt.setCompoundTag("nbt", StackUtil.getOrCreateNbtData(player.inventory.armorInventory[0]));
					}
					player.inventory.armorInventory[0] = null;
					ClientTickHandleric2ca.bootsNBT = bootsnbt;
				}
				ClientTickHandleric2ca.isCloakActiveByMod = true;
				ItemNBTHelper.saveCloakStatus(stack, true);
			}
		}
		return true;
	}

	public static boolean switchFlyModeClient(EntityPlayer player, ItemStack stack)
	{
		if (ItemNBTHelper.readFlyStatus(stack))
		{
			if (!player.capabilities.isCreativeMode)
			{
				player.capabilities.allowFlying = false;
				player.capabilities.isFlying = false;
			}
			ClientProxyic2ca.sendPlayerMessage(player, "Flight Turbine disabled.");
			ClientTickHandleric2ca.isFlyActiveByMod = false;
			ItemNBTHelper.saveFlyStatus(stack, false);
		}
		else
		{
			int var2 = ItemNBTHelper.getCharge(stack);
			if (var2 < 10 && !player.capabilities.isCreativeMode)
			{
				ClientProxyic2ca.sendPlayerMessage(player, "Not enough energy to enable flight mode!");
			}
			else
			{
				ClientProxyic2ca.sendPlayerMessage(player, "Flight Turbine enabled.");
				player.capabilities.allowFlying = true;
				player.capabilities.isFlying = true;
				ClientTickHandleric2ca.isFlyActiveByMod = true;
				ItemNBTHelper.saveFlyStatus(stack, true);
			}
		}
		return true;
	}
}
