package ic2ca.common;

import ic2.api.recipe.IRecipeInput;
import ic2.api.recipe.RecipeInputItemStack;
import ic2.api.recipe.RecipeOutput;
import ic2ca.api.IArmorAssemblerRecipeManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cpw.mods.fml.common.ICraftingHandler;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.item.ItemStack;

public class ArmorAssemblerRecipes implements IArmorAssemblerRecipeManager
{
	private static final ArmorAssemblerRecipes assemblyCore = new ArmorAssemblerRecipes();
	private final Map<IArmorAssemblerRecipeManager.Input, RecipeOutput> recipes = new HashMap();
	private static List<Integer> items = new ArrayList();

	public static void addAssemblyRecipe(ItemStack result, ItemStack i1, ItemStack i2)
	{
		assemblyCore.addRecipe(result, new RecipeInputItemStack(i1, 1), new RecipeInputItemStack(i2, 1));
		items.add(Integer.valueOf(i1.itemID));
		items.add(Integer.valueOf(i2.itemID));
	}

	@Override
	public void addRecipe(ItemStack result, IRecipeInput i1, IRecipeInput i2) 
	{
		if (result == null) {
			throw new NullPointerException("The recipe output is null");
		}		
		if (i1 == null) {
			throw new NullPointerException("The I1 recipe input is null");
		}
		if (i2 == null) {
			throw new NullPointerException("The I2 recipe input is null");
		}
		ICraftingHandler test = IC2CA.instance;
		InventoryBasic inv = new InventoryBasic(null,false,2);
		ItemStack output = result.copy();
		inv.setInventorySlotContents(0,i1.getInputs().get(0));
		inv.setInventorySlotContents(1,i2.getInputs().get(0));		
		test.onCrafting(null, output, inv);
		this.recipes.put(new IArmorAssemblerRecipeManager.Input(i1, i2), new RecipeOutput(null, new ItemStack[] { output }));
		this.recipes.put(new IArmorAssemblerRecipeManager.Input(i2, i1), new RecipeOutput(null, new ItemStack[] { output }));
	}

	public static final ArmorAssemblerRecipes assembly()
	{
		return assemblyCore;
	}

	public static int[] getItemList()
	{
		int[] ret = new int[items.size()];
		int i = 0;
		for (Integer e : items)  
			ret[i++] = e.intValue();
		return ret;
	}  

	@Override
	public RecipeOutput getAssemblyResult(ItemStack item1, ItemStack item2) 
	{
		if ((item1 == null) || (item2 == null)) {
			return null;
		}
		for (Map.Entry<IArmorAssemblerRecipeManager.Input, RecipeOutput> entry : this.recipes.entrySet())
		{
			IArmorAssemblerRecipeManager.Input recipeInput = (IArmorAssemblerRecipeManager.Input)entry.getKey();
			if (recipeInput.matches(item1, item2)) {
				return (RecipeOutput)entry.getValue();
			}
		}
		return null;
	}

	@Override
	public Map<Input, RecipeOutput> getRecipes() 
	{
		return this.recipes;
	}
}
