package ic2ca.common.item;

import ic2.api.item.ElectricItem;
import ic2.api.item.IElectricItem;
import ic2.core.IC2;
import ic2.core.util.StackUtil;
import ic2ca.common.IC2CA;
import ic2ca.common.IItemUpgradeable;
import ic2ca.common.Ic2caItems;
import ic2ca.common.Util;
import ic2ca.common.entity.EntityTechArrow;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.Icon;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.ArrowLooseEvent;
import net.minecraftforge.event.entity.player.ArrowNockEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemNanoBow extends Item implements IElectricItem, IItemUpgradeable
{
	public Icon[] icons;

	public ItemNanoBow(int par1, String name)
	{
		super(par1);
		this.maxStackSize = 1;
		this.setMaxDamage(27);
		this.setFull3D();
		this.setUnlocalizedName(name);    
		this.setCreativeTab(IC2CA.tabIC2CA);
		GameRegistry.registerItem(this, name);
		this.icons = new Icon[4];
	}

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List info, boolean par4)
	{
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(par1ItemStack);
		IElectricItem elec = (IElectricItem)par1ItemStack.getItem();
		if (!nbt.getBoolean("loaded"))
		{
			if (nbt.getInteger("tier") == 0) {
				nbt.setInteger("tier", elec.getTier(par1ItemStack));
			}
			if (nbt.getInteger("transferLimit") == 0) {
				nbt.setInteger("transferLimit", elec.getTransferLimit(par1ItemStack));
			}
			if (nbt.getInteger("maxCharge") == 0) {
				nbt.setInteger("maxCharge", elec.getMaxCharge(par1ItemStack));
			}
			nbt.setBoolean("loaded", true);
		}
		if (nbt.getInteger("transferLimit") != elec.getTransferLimit(par1ItemStack)) {
			info.add(String.format(StatCollector.translateToLocal("info.transferspeed"), nbt.getInteger("transferLimit")));
		}
		if (nbt.getInteger("tier") != elec.getTier(par1ItemStack)) {
			info.add(String.format(StatCollector.translateToLocal("info.chargingtier"), nbt.getInteger("tier")));
		}
		/*
		int charge = nbt.getInteger("charge");
		int maxcharge = nbt.getInteger("maxCharge");
		info.add(charge + "/" + maxcharge + " EU");*/
	}

	@Override
	public boolean canProvideEnergy(ItemStack is)
	{
		return false;
	}

	@Override
	public int getChargedItemId(ItemStack is)
	{
		return this.itemID;
	}

	@Override
	public int getDefaultMaxCharge()
	{
		return 40000;
	}

	@Override
	public int getDefaultTier()
	{
		return 2;
	}

	@Override
	public int getDefaultTransferLimit()
	{
		return 128;
	}

	@Override
	public int getEmptyItemId(ItemStack is)
	{
		return this.itemID;
	}

	@Override
	public Icon getIcon(ItemStack stack, int renderPass, EntityPlayer player, ItemStack usingItem, int useRemaining)
	{
		if ((usingItem != null) && (usingItem.getItem().itemID == Ic2caItems.nanoBow.itemID))
		{
			NBTTagCompound nbt = StackUtil.getOrCreateNbtData(stack);
			int mode = nbt.getInteger("bowMode");
			int i1 = 18;
			int i2 = 13;
			if ((mode == 4) || (mode == 6))
			{
				i1 = 36;
				i2 = 26;
			}
			else if (mode == 2)
			{
				i1 = 5;
				i2 = 3;
			}
			int k = usingItem.getMaxItemUseDuration() - useRemaining;
			if (k >= i1) {
				return this.icons[3];
			}
			if (k > i2) {
				return this.icons[2];
			}
			if (k > 0) {
				return this.icons[1];
			}
		}
		return this.icons[0];
	}

	@Override
	public boolean getIsRepairable(ItemStack var1, ItemStack var2)
	{
		return false;
	}

	@Override
	public int getItemEnchantability()
	{
		return 0;
	}

	@Override
	public int getItemTier()
	{
		return 3;
	}

	@Override
	public EnumAction getItemUseAction(ItemStack par1ItemStack)
	{
		return EnumAction.bow;
	}

	@Override
	public int getMaxCharge(ItemStack itemstack)
	{
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(itemstack);
		if (nbt.getInteger("maxCharge") == 0) {
			nbt.setInteger("maxCharge", getDefaultMaxCharge());
		}
		return nbt.getInteger("maxCharge");
	}

	@Override
	public int getMaxItemUseDuration(ItemStack par1ItemStack)
	{
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(par1ItemStack);
		int mode = nbt.getInteger("bowMode");
		if ((mode == 4) || (mode == 6)) {
			return 144000;
		}
		if (mode == 2) {
			return 18000;
		}
		return 72000;
	}

	@Override
	public int getMaxUpgradeableCharge()
	{
		return IC2CA.maxEnergyUpgrades - getDefaultMaxCharge();
	}

	@Override
	public int getMaxUpgradeableTransfer()
	{
		return IC2CA.maxTransferUpgrades - getDefaultTransferLimit();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack itemstack)
	{
		return EnumRarity.uncommon;
	}

	@Override
	public void getSubItems(int var1, CreativeTabs var2, List itemList)
	{
		ItemStack charged = new ItemStack(this, 1);
		ElectricItem.manager.charge(charged, 2147483647, 2147483647, true, false);
		itemList.add(charged);
		itemList.add(new ItemStack(this, 1, getMaxDamage()));
	}

	@Override
	public int getTier(ItemStack itemstack)
	{
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(itemstack);
		if (nbt.getInteger("tier") == 0) {
			nbt.setInteger("tier", getDefaultTier());
		}
		return nbt.getInteger("tier");
	}

	@Override
	public int getTransferLimit(ItemStack itemstack)
	{
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(itemstack);
		if (nbt.getInteger("transferLimit") == 0) {
			nbt.setInteger("transferLimit", getDefaultTransferLimit());
		}
		return nbt.getInteger("transferLimit");
	}

	@Override
	public boolean hitEntity(ItemStack par1ItemStack, EntityLivingBase par2EntityLiving, EntityLivingBase par3EntityLiving)
	{
		return true;
	}

	@Override
	public boolean isRepairable()
	{
		return false;
	}

	@Override
	public boolean onBlockDestroyed(ItemStack par1ItemStack, World par2World, int par3, int par4, int par5, int par6, EntityLivingBase par7EntityLiving)
	{
		return true;
	}

	public ItemStack onFoodEaten(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
	{
		return par1ItemStack;
	}

	@Override
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
	{
		ArrowNockEvent event = new ArrowNockEvent(par3EntityPlayer, par1ItemStack);
		MinecraftForge.EVENT_BUS.post(event);
		if (event.isCanceled()) {
			return event.result;
		}
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(par1ItemStack);
		if ((IC2.keyboard.isModeSwitchKeyDown(par3EntityPlayer)) && (nbt.getByte("toggleTimer") == 0))
		{
			if (IC2.platform.isSimulating())
			{
				byte toggle = 10;
				nbt.setByte("toggleTimer", toggle);

				int mode = nbt.getInteger("bowMode");
				mode++;
				if (mode == 2 && !IC2CA.rapidFireMode) mode++;
				if (mode == 3 && !IC2CA.spreadMode) mode++;
				if (mode == 4 && !IC2CA.sniperMode) mode++;
				if (mode == 5 && !IC2CA.flameMode) mode++;
				if (mode == 6 && !IC2CA.explosiveMode) mode++;
				if (mode > 6) {
					mode -= 6;
				}
				nbt.setInteger("bowMode", mode);

				String[] name = { StatCollector.translateToLocal("info.normal"), 
						StatCollector.translateToLocal("info.rapidfire"), 
						StatCollector.translateToLocal("info.spread"), 
						StatCollector.translateToLocal("info.sniper"), 
						StatCollector.translateToLocal("info.flame"), 
						StatCollector.translateToLocal("info.explosive")
					};
				IC2.platform.messagePlayer(par3EntityPlayer, String.format(StatCollector.translateToLocal("info.modeenabled"), name[(mode - 1)]));
			}
		}
		else if ((par3EntityPlayer.capabilities.isCreativeMode) || (ElectricItem.manager.canUse(par1ItemStack, 100))) {
			par3EntityPlayer.setItemInUse(par1ItemStack, getMaxItemUseDuration(par1ItemStack));
		}
		return par1ItemStack;
	}

	@Override
	public void onPlayerStoppedUsing(ItemStack par1ItemStack, World par2World, EntityPlayer player, int par4)
	{
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(par1ItemStack);
		int mode = nbt.getInteger("bowMode");

		int var6 = getMaxItemUseDuration(par1ItemStack) - par4;

		ArrowLooseEvent event = new ArrowLooseEvent(player, par1ItemStack, var6);
		MinecraftForge.EVENT_BUS.post(event);
		if (event.isCanceled()) {
			return;
		}
		var6 = event.charge;
		if ((mode == 4) || (mode == 6)) {
			var6 /= 2;
		}
		if (mode == 2) {
			var6 *= 4;
		}
		float var7 = var6 / 20.0F;
		var7 = (var7 * var7 + var7 * 2.0F) / 3.0F;
		if (var7 < 0.1D) {
			return;
		}
		if (var7 > 1.0F) {
			var7 = 1.0F;
		}
		EntityTechArrow var8 = new EntityTechArrow(par2World, player, var7 * 2.0F);
		EntityTechArrow arrow2 = new EntityTechArrow(par2World, player, var7 * 2.0F);
		arrow2.setPosition(arrow2.posX, arrow2.posY + 0.5D, arrow2.posZ);
		arrow2.canBePickedUp = 2;
		EntityTechArrow arrow3 = new EntityTechArrow(par2World, player, var7 * 2.0F);
		arrow3.setPosition(arrow3.posX, arrow3.posY + 0.25D, arrow3.posZ);
		arrow3.canBePickedUp = 2;
		EntityTechArrow arrow4 = new EntityTechArrow(par2World, player, var7 * 2.0F);
		arrow4.setPosition(arrow4.posX, arrow4.posY - 0.25D, arrow4.posZ);
		arrow4.canBePickedUp = 2;
		EntityTechArrow arrow5 = new EntityTechArrow(par2World, player, var7 * 2.0F);
		arrow5.setPosition(arrow5.posX, arrow5.posY - 0.5D, arrow5.posZ);
		arrow5.canBePickedUp = 2;
		if (var7 == 1.0F) {
			var8.setIsCritical(true);
		}
		int var9 = EnchantmentHelper.getEnchantmentLevel(Enchantment.power.effectId, par1ItemStack);
		if ((mode == 1) && (var8.getIsCritical())) {
			var9 += 3;
		} else if ((mode == 4) && (var8.getIsCritical())) {
			var9 += 10;
		}
		if (var9 > 0) {
			var8.setDamage(var8.getDamage() + var9 * 0.5D + 0.5D);
		}
		if (IC2CA.nanoBowBoost > 0) {
			var8.setDamage(var8.getDamage() + IC2CA.nanoBowBoost * 0.5D + 0.5D);
		}
		int var10 = EnchantmentHelper.getEnchantmentLevel(Enchantment.punch.effectId, par1ItemStack);
		if ((mode == 1) && (var8.getIsCritical())) {
			var10 += 1;
		} else if ((mode == 4) && (var8.getIsCritical())) {
			var10 += 5;
		}
		if (var10 > 0) {
			var8.setKnockbackStrength(var10);
		}
		if (EnchantmentHelper.getEnchantmentLevel(Enchantment.flame.effectId, par1ItemStack) > 0) {
			var8.setFire(100);
		}
		if ((mode == 5) && (var8.getIsCritical())) {
			var8.setFire(2000);
		}
		if ((mode == 6) && (var8.getIsCritical())) {
			var8.setExplosive(true);
		}
		par2World.playSoundAtEntity(player, "random.bow", 1.0F, 1.0F / (itemRand.nextFloat() * 0.4F + 1.2F) + var7 * 0.5F);
		var8.canBePickedUp = 2;
		if (!par2World.isRemote) {
			if (mode == 1)
			{
				ElectricItem.manager.use(par1ItemStack, 100, player);
				par2World.spawnEntityInWorld(var8);
			}
			else if (mode == 2)
			{
				ElectricItem.manager.use(par1ItemStack, 50, player);
				par2World.spawnEntityInWorld(var8);
			}
			else if (mode == 3)
			{
				ElectricItem.manager.use(par1ItemStack, 250, player);
				par2World.spawnEntityInWorld(var8);
				if (var8.getIsCritical())
				{
					par2World.spawnEntityInWorld(arrow2);
					par2World.spawnEntityInWorld(arrow3);
					par2World.spawnEntityInWorld(arrow4);
					par2World.spawnEntityInWorld(arrow5);
				}
			}
			else if (mode == 4)
			{
				ElectricItem.manager.use(par1ItemStack, 250, player);
				par2World.spawnEntityInWorld(var8);
			}
			else if (mode == 5)
			{
				ElectricItem.manager.use(par1ItemStack, 100, player);
				par2World.spawnEntityInWorld(var8);
			}
			else if (mode == 6)
			{
				ElectricItem.manager.use(par1ItemStack, 250, player);
				par2World.spawnEntityInWorld(var8);
			}
		}
	}

	@Override
	public void onUpdate(ItemStack par1ItemStack, World world, Entity entity, int par4, boolean par5)
	{
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(par1ItemStack);
		byte toggle = nbt.getByte("toggleTimer");
		if (toggle > 0)
		{
			toggle = (byte)(toggle - 1);
			nbt.setByte("toggleTimer", toggle);
		}
		int mode = nbt.getInteger("bowMode");
		if (mode == 0) {
			nbt.setInteger("bowMode", 1);
		}
	}

	@Override
	public void onUsingItemTick(ItemStack itemstack, EntityPlayer player, int i)
	{
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(itemstack);
		int mode = nbt.getInteger("bowMode");
		if (mode == 2)
		{
			int j = getMaxItemUseDuration(itemstack) - i;
			if ((j >= 5) && (ElectricItem.manager.canUse(itemstack, 50))) {
				player.stopUsingItem();
			}
		}
	}

	@Override
	public void registerIcons(IconRegister ir)
	{
		this.icons[0] = Util.register(ir, this);
		this.icons[1] = Util.register(ir, Util.getFileName(this) + "_1");
		this.icons[2] = Util.register(ir, Util.getFileName(this) + "_2");
		this.icons[3] = Util.register(ir, Util.getFileName(this) + "_3");
		this.itemIcon = this.icons[0];
	}
}
