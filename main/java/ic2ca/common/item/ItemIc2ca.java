package ic2ca.common.item;

import ic2ca.common.IC2CA;
import ic2ca.common.Util;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemIc2ca extends Item
{
	int raritynum = 0;

	public ItemIc2ca(int id, String name)
	{
		super(id);
		setUnlocalizedName(name);    
		setCreativeTab(IC2CA.tabIC2CA);
		GameRegistry.registerItem(this, name);    
	}

	public ItemIc2ca(int id, String name, int stackSize, int rarity)
	{
		super(id);
		this.maxStackSize = stackSize;
		this.raritynum = rarity;
		setUnlocalizedName(name);    
		setCreativeTab(IC2CA.tabIC2CA);
		GameRegistry.registerItem(this, name);    
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack itemstack)
	{
		if (this.raritynum == 2) {
			return EnumRarity.uncommon;
		}
		if (this.raritynum == 3) {
			return EnumRarity.rare;
		}
		if (this.raritynum == 4) {
			return EnumRarity.epic;
		}
		return EnumRarity.common;
	}

	@Override
	public void registerIcons(IconRegister ir)
	{
		this.itemIcon = Util.register(ir, this);
	}
}
