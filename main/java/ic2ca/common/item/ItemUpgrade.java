package ic2ca.common.item;

import ic2ca.common.IC2CA;
import ic2ca.common.IItemUpgrade;
import ic2ca.common.Ic2caItems;
import ic2ca.common.Util;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemUpgrade extends Item implements IItemUpgrade
{
	int raritynum = 0;
	private EnumUpgradeType type;

	public ItemUpgrade(int id, String name, int stackSize, int rarity, EnumUpgradeType at)
	{
		super(id);
		this.raritynum = rarity;
		this.maxStackSize = stackSize;
		setUnlocalizedName(name);    
		setCreativeTab(IC2CA.tabIC2CA);
		GameRegistry.registerItem(this, name);     
		this.type = at;
	}

    @SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack is, EntityPlayer player, List list, boolean par4)
	{
		if (this.type != null) 
		{
			list.add(String.format(StatCollector.translateToLocal("info.compatableall"), StatCollector.translateToLocal(this.type.name)));
		}
	}

    @Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack itemstack)
	{
		if (this.raritynum == 2) 
		{
			return EnumRarity.uncommon;
		}
		if (this.raritynum == 3) 
		{
			return EnumRarity.rare;
		}
		if (this.raritynum == 4) 
		{
			return EnumRarity.epic;
		}
		return EnumRarity.common;
	}

	public int getStackModifier(Item item)
	{
		if ((item == Ic2caItems.flightModule.getItem()) || (item == Ic2caItems.cloakingModule.getItem()) || (item == Ic2caItems.overchargeModule.getItem())) {
			return 16;
		}
		return 1;
	}

    @Override
	public void registerIcons(IconRegister ir)
	{
		this.itemIcon = Util.register(ir, this);
	}
}
