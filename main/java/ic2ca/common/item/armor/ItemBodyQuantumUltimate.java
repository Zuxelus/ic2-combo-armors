package ic2ca.common.item.armor;

import ic2.api.item.IMetalArmor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemBodyQuantumUltimate extends ItemArmorBaseJetpack implements IMetalArmor
{
	public ItemBodyQuantumUltimate(int id, String name, int renderIndex)
	{
		super(id, name, renderIndex, 1, 18000000, 12000, 4, true);
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
		return "ic2ca:textures/armor/ultimatequantum_1.png";
	}

	@Override
	public double getDamageAbsorptionRatio()
	{
		return 1.1D;
	}

	@Override
	public int getEnergyPerDamage()
	{
		return 900;
	}

	@Override
	public int getItemTier()
	{
		return 6;
	}
	
	@Override	
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack var1)
	{
		return EnumRarity.epic;
	}

	@Override
	public boolean isMetalArmor(ItemStack var1, EntityPlayer var2)
	{
		return true;
	}

	@Override
	public void onArmorTickUpdate(World world, EntityPlayer player, ItemStack var2)
	{
		player.extinguish();
		super.onArmorTickUpdate(world, player, var2);
	}
}
