package ic2ca.common.item.armor;

import ic2.api.item.ElectricItem;
import ic2.api.item.IElectricItem;
import ic2.api.item.IMetalArmor;
import ic2.core.IC2;
import ic2.core.util.StackUtil;
import ic2ca.common.IC2CA;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemBootsStaticNano extends ItemArmorElectricUtility implements IMetalArmor
{
	public ItemBootsStaticNano(int id, String name, int renderIndex)
	{
		super(id, name, renderIndex, 3, 1000000, 1000, 3, false);

		MinecraftForge.EVENT_BUS.register(this);
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
		return "ic2ca:textures/armor/nanostatic_1.png";
	}

	@Override
	public double getDamageAbsorptionRatio()
	{
		return 0.9D;
	}

	@Override
	public int getEnergyPerDamage()
	{
		return 800;
	}

	@Override
	public int getItemTier()
	{
		return 3;
	}

	@Override
	public ArmorProperties getProperties(EntityLivingBase player, ItemStack armor, DamageSource source, double damage, int slot)
	{
		if (source == DamageSource.fall)
		{
			int var7 = getEnergyPerDamage();
			int var8 = var7 > 0 ? 25 * ElectricItem.manager.discharge(armor, 2147483647, 2147483647, true, true) / var7 : 0;
			return new ArmorProperties(10, damage < 8.0D ? 1.0D : 0.875D, var8);
		}
		return super.getProperties(player, armor, source, damage, slot);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack var1)
	{
		return EnumRarity.uncommon;
	}

	@Override
	public boolean isMetalArmor(ItemStack var1, EntityPlayer var2)
	{
		return true;
	}

	@Override
	public void onArmorTickUpdate(World world, EntityPlayer player, ItemStack var2)
	{
		boolean stuff = false;
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(var2);
		int prod = 0;
		if (nbt.getInteger("staticProd") > 0) {
			prod = nbt.getInteger("staticProd");
		}
		boolean var4 = (player.ridingEntity != null) || (player.isInWater());
		if ((!nbt.hasKey("x")) || (var4)) {
			nbt.setInteger("x", (int)player.posX);
		}
		if ((!nbt.hasKey("z")) || (var4)) {
			nbt.setInteger("z", (int)player.posZ);
		}
		double var5 = Math.sqrt((nbt.getInteger("x") - (int)player.posX) * (nbt.getInteger("x") - (int)player.posX) + (nbt.getInteger("z") - (int)player.posZ) * (nbt.getInteger("z") - (int)player.posZ));
		if (var5 >= 5.0D)
		{
			nbt.setInteger("x", (int)player.posX);
			nbt.setInteger("z", (int)player.posZ);
			if ((player.inventory.armorInventory[IC2CA.stPriority1] != null) && ((player.inventory.armorInventory[IC2CA.stPriority1].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeStatic(player, IC2CA.stPriority1, var5, prod))) {
				stuff = true;
			} else if ((player.inventory.armorInventory[IC2CA.stPriority2] != null) && ((player.inventory.armorInventory[IC2CA.stPriority2].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeStatic(player, IC2CA.stPriority2, var5, prod))) {
				stuff = true;
			} else if ((player.inventory.armorInventory[IC2CA.stPriority3] != null) && ((player.inventory.armorInventory[IC2CA.stPriority3].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeStatic(player, IC2CA.stPriority3, var5, prod))) {
				stuff = true;
			} else if ((player.inventory.armorInventory[IC2CA.stPriority4] != null) && ((player.inventory.armorInventory[IC2CA.stPriority4].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeStatic(player, IC2CA.stPriority4, var5, prod))) {
				stuff = true;
			} else {
				stuff = false;
			}
		}
		else
		{
			stuff = false;
		}
		if (stuff) {
			player.openContainer.detectAndSendChanges();
		}
	}

	@ForgeSubscribe
	public void onEntityLivingFallEvent(LivingFallEvent event)
	{
		if ((IC2.platform.isSimulating()) && ((event.entity instanceof EntityLivingBase)))
		{
			EntityLivingBase entity = (EntityLivingBase)event.entity;
			ItemStack armor = entity.getCurrentItemOrArmor(1);
			//			ItemStack armor = entity.inventory.armorInventory[0];	      
			if ((armor != null) && (armor.getItem() == this))
			{
				int fallDamage = (int)event.distance - 3;
				if (fallDamage >= 8) {
					return;
				}
				int energyCost = getEnergyPerDamage() * fallDamage;
				if (energyCost <= ElectricItem.manager.getCharge(armor))
				{
					ElectricItem.manager.discharge(armor, energyCost, 2147483647, true, false);

					event.setCanceled(true);
				}
			}
		}
	}
}
