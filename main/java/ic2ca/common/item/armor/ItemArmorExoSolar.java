package ic2ca.common.item.armor;

import ic2.api.item.IElectricItem;
import ic2.core.block.generator.tileentity.TileEntitySolarGenerator;
import ic2.core.util.StackUtil;
import ic2ca.common.IC2CA;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class ItemArmorExoSolar extends ItemArmorExoUtility
{
	public ItemArmorExoSolar(int id, String name, int renderIndex)
	{
		super(id, name, renderIndex, 0);
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
		return "ic2ca:textures/armor/exosolar_1.png";
	}

	@Override
	public int getItemEnchantability()
	{
		return 0;
	}

	@Override
	public int getItemTier()
	{
		return 1;
	}

	@Override
	public void onArmorTickUpdate(World world, EntityPlayer player, ItemStack itemstack)
	{
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(itemstack);

		boolean stuff = false;
		int prod;
		if (nbt.getInteger("solarProd") > 0) {
			prod = nbt.getInteger("solarProd") + 1;
		} else {
			prod = 1;
		}
		int var1 = (int) player.posX;
		int var2 = (int) player.posY;
		int var3 = (int) player.posZ;
		
		if (!world.isRemote && TileEntitySolarGenerator.isSunVisible(player.worldObj, (int)player.posX, (int)player.posY + 1, (int)player.posZ))
		{
			if ((player.inventory.armorInventory[IC2CA.soPriority1] != null) && ((player.inventory.armorInventory[IC2CA.soPriority1].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeSolar(player, IC2CA.soPriority1, prod))) {
				stuff = true;
			} else if ((player.inventory.armorInventory[IC2CA.soPriority2] != null) && ((player.inventory.armorInventory[IC2CA.soPriority2].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeSolar(player, IC2CA.soPriority2, prod))) {
				stuff = true;
			} else if ((player.inventory.armorInventory[IC2CA.soPriority3] != null) && ((player.inventory.armorInventory[IC2CA.soPriority3].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeSolar(player, IC2CA.soPriority3, prod))) {
				stuff = true;
			} else if ((player.inventory.armorInventory[IC2CA.soPriority4] != null) && ((player.inventory.armorInventory[IC2CA.soPriority4].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeSolar(player, IC2CA.soPriority4, prod))) {
				stuff = true;
			} else {
				stuff = false;
			}
		}
		else {
			stuff = false;
		}
		if (stuff) {
			player.inventoryContainer.detectAndSendChanges();
		}
	}
}
