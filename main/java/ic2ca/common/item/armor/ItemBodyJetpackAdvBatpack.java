package ic2ca.common.item.armor;

import net.minecraft.entity.Entity;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemBodyJetpackAdvBatpack extends ItemArmorBaseJetpack
{
	public ItemBodyJetpackAdvBatpack(int id, String name, int renderIndex)
	{
		super(id, name, renderIndex, 1, 650000, 1000, 3, true);
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
		return "ic2ca:textures/armor/advjetpack_1.png";
	}

	@Override
	public int getItemTier()
	{
		return 3;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack var1)
	{
		return EnumRarity.uncommon;
	}
}
