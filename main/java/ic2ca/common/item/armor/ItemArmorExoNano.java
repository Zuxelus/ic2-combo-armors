package ic2ca.common.item.armor;

import ic2.api.item.ElectricItem;
import ic2.api.item.IMetalArmor;
import ic2.core.IC2;
import ic2ca.common.Ic2caItems;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemArmorExoNano extends ItemArmorElectricUtility implements IMetalArmor
{
	public ItemArmorExoNano(int id, String name, int renderIndex, int piece)
	{
		super(id, name, renderIndex, piece, 1000000, 1600, 3, false);
		if (piece == 3) {
			MinecraftForge.EVENT_BUS.register(this);
		}
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
		if ((stack.itemID == Ic2caItems.exoNanoHelm.itemID) || (stack.itemID == Ic2caItems.exoNanoChest.itemID) || (stack.itemID == Ic2caItems.exoNanoBoots.itemID)) {
			return "ic2ca:textures/armor/exonano_1.png";
		}
		return "ic2ca:textures/armor/exonano_2.png";
	}

	@Override
	public double getDamageAbsorptionRatio()
	{
		return 0.9D;
	}

	@Override
	public int getEnergyPerDamage()
	{
		return 800;
	}

	@Override
	public int getItemTier()
	{
		return 3;
	}

	@Override
	public ArmorProperties getProperties(EntityLivingBase player, ItemStack armor, DamageSource source, double damage, int slot)  
	{
		if ((source == DamageSource.fall) && (this.armorType == 3))
		{
			int var7 = getEnergyPerDamage();
			int var8 = var7 > 0 ? 25 * ElectricItem.manager.discharge(armor, 2147483647, 2147483647, true, true) / var7 : 0;
			return new ArmorProperties(10, damage < 8.0D ? 1.0D : 0.875D, var8);
		}
		return super.getProperties(player, armor, source, damage, slot);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public EnumRarity getRarity(ItemStack var1)
	{
		return EnumRarity.uncommon;
	}

	@Override
	public boolean isMetalArmor(ItemStack var1, EntityPlayer var2)
	{
		return true;
	}

	@ForgeSubscribe
	public void onEntityLivingFallEvent(LivingFallEvent event)
	{
		if ((IC2.platform.isSimulating()) && ((event.entity instanceof EntityLivingBase)))
		{
			EntityLivingBase entity = (EntityLivingBase)event.entity;
			ItemStack armor = entity.getCurrentItemOrArmor(1);
			if ((armor != null) && (armor.getItem() == this))
			{
				int fallDamage = (int)event.distance - 3;
				if (fallDamage >= 8) {
					return;
				}
				int energyCost = getEnergyPerDamage() * fallDamage;
				if (energyCost <= ElectricItem.manager.getCharge(armor))
				{
					ElectricItem.manager.discharge(armor, energyCost, 2147483647, true, false);
					event.setCanceled(true);
				}
			}
		}
	}
}
