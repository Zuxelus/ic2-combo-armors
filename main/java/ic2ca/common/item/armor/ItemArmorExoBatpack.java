package ic2ca.common.item.armor;

import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;

public class ItemArmorExoBatpack extends ItemArmorElectricUtility
{
	public ItemArmorExoBatpack(int id, String name, int renderIndex)
	{
		super(id, name, renderIndex, 1, 60000, 100, 1, true);
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
		return "ic2ca:textures/armor/exobatpack_1.png";
	}

	@Override
	public double getDamageAbsorptionRatio()
	{
		return 0.0D;
	}

	@Override
	public int getEnergyPerDamage()
	{
		return 0;
	}

	@Override
	public int getItemTier()
	{
		return 1;
	}
}
