package ic2ca.common.item.armor;

import ic2.api.item.IElectricItem;
import ic2.api.item.IMetalArmor;
import ic2.core.util.StackUtil;
import ic2ca.common.IC2CA;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class ItemArmorExoStatic extends ItemArmorExoUtility implements IMetalArmor
{
	public ItemArmorExoStatic(int id, String name, int renderIndex)
	{
		super(id, name, renderIndex, 3);
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
		return "ic2ca:textures/armor/exostatic_1.png";
	}

	@Override
	public int getItemTier()
	{
		return 1;
	}

	@Override
	public boolean isMetalArmor(ItemStack var1, EntityPlayer player)
	{
		return true;
	}

	@Override
	public void onArmorTickUpdate(World world, EntityPlayer player, ItemStack itemStack)
	{
		boolean ret = false;
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(itemStack);
		int prod = 0;
		if (nbt.getInteger("staticProd") > 0) {
			prod = nbt.getInteger("staticProd");
		}
		boolean isNotWalking = (player.ridingEntity != null) || (player.isInWater());
		if ((!nbt.hasKey("x")) || (isNotWalking)) {
			nbt.setInteger("x", (int)player.posX);
		}
		if ((!nbt.hasKey("z")) || (isNotWalking)) {
			nbt.setInteger("z", (int)player.posZ);
		}
		double distance = Math.sqrt((nbt.getInteger("x") - (int)player.posX) * (nbt.getInteger("x") - (int)player.posX) + (nbt.getInteger("z") - (int)player.posZ) * (nbt.getInteger("z") - (int)player.posZ));
		if (distance >= 5.0D)
		{
			nbt.setInteger("x", (int)player.posX);
			nbt.setInteger("z", (int)player.posZ);
			if ((player.inventory.armorInventory[IC2CA.stPriority1] != null) && ((player.inventory.armorInventory[IC2CA.stPriority1].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeStatic(player, IC2CA.stPriority1, distance, prod))) {
				ret = true;
			} else if ((player.inventory.armorInventory[IC2CA.stPriority2] != null) && ((player.inventory.armorInventory[IC2CA.stPriority2].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeStatic(player, IC2CA.stPriority2, distance, prod))) {
				ret = true;
			} else if ((player.inventory.armorInventory[IC2CA.stPriority3] != null) && ((player.inventory.armorInventory[IC2CA.stPriority3].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeStatic(player, IC2CA.stPriority3, distance, prod))) {
				ret = true;
			} else if ((player.inventory.armorInventory[IC2CA.stPriority4] != null) && ((player.inventory.armorInventory[IC2CA.stPriority4].getItem() instanceof IElectricItem)) && (IC2CA.tryChargeStatic(player, IC2CA.stPriority4, distance, prod))) {
				ret = true;
			} else {
				ret = false;
			}
		}
		else
		{
			ret = false;
		}
		if (ret) {
			player.inventoryContainer.detectAndSendChanges();
		}
	}
}
