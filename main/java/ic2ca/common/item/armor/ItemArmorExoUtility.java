package ic2ca.common.item.armor;

import ic2.core.util.StackUtil;
import ic2ca.common.IC2CA;
import ic2ca.common.IItemUpgradeable;
import ic2ca.common.Util;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.StatCollector;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public abstract class ItemArmorExoUtility extends ItemArmor implements IItemUpgradeable
{
	public ItemArmorExoUtility(int id, String name, int renderIndex, int piece)
	{
		super(id, EnumArmorMaterial.DIAMOND, renderIndex, piece);
		setMaxDamage(0);
		setMaxStackSize(1);
		setUnlocalizedName(name);
		setCreativeTab(IC2CA.tabIC2CA);
		GameRegistry.registerItem(this, name);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
	{
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(par1ItemStack);
		par3List.add(StatCollector.translateToLocal("info.upgrademoduleinstalled"));
		if (nbt.getBoolean("flight")) 
		{
			par3List.add(StatCollector.translateToLocal("info.flightturbineinstalled"));
		}
		if (nbt.getBoolean("cloaking")) 
		{
			par3List.add(StatCollector.translateToLocal("info.cloakingmoduleinstalled"));
		}
		if (nbt.getBoolean("overcharge")) 
		{
			par3List.add(StatCollector.translateToLocal("info.dischargemoduleinstalled"));
		}
		if (nbt.getInteger("solarProd") > 0)
		{
			int i = nbt.getInteger("solarProd") + 1;
			par3List.add(String.format(StatCollector.translateToLocal("info.solarproduces"), i));
		}
		if (nbt.getInteger("staticProd") > 0)
		{
			int i = nbt.getInteger("staticProd") + 1;
			par3List.add(String.format(StatCollector.translateToLocal("info.staticproduces"), i));
		}
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
		return null;
	}

	@Override  
	public int getDefaultMaxCharge()
	{
		return 0;
	}

	@Override
	public int getDefaultTier()
	{
		return 0;
	}

	@Override
	public int getDefaultTransferLimit()
	{
		return 0;
	}

	@Override
	public boolean getIsRepairable(ItemStack var1, ItemStack var2)
	{
		return false;
	}

	@Override
	public int getItemEnchantability()
	{
		return 0;
	}

	@Override
	public int getMaxUpgradeableCharge()
	{
		return 0;
	}

	@Override
	public int getMaxUpgradeableTransfer()
	{
		return 0;
	}

	@Override
	public void getSubItems(int var1, CreativeTabs var2, List var3)
	{
		var3.add(new ItemStack(this, 1, getMaxDamage()));
	}

	@Override
	public boolean isRepairable()
	{
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister ir)
	{
		this.itemIcon = Util.register(ir, this);
	}
}
