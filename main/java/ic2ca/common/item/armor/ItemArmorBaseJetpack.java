package ic2ca.common.item.armor;

import ic2.api.item.ElectricItem;
import ic2.core.IC2;
import ic2.core.audio.AudioSource;
import ic2.core.audio.PositionSpec;
import ic2.core.util.StackUtil;
import ic2ca.common.IC2CA;

import java.util.List;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public abstract class ItemArmorBaseJetpack extends ItemArmorElectricUtility implements IJetpack
{
	public static boolean lastJetpackUsed = false;
	public static AudioSource audioSource;

	public ItemArmorBaseJetpack(int id, String name, int renderIndex, int piece, int maxCharge, int transferLimit, int tier, boolean share)
	{
		super(id, name, renderIndex,  piece, maxCharge, transferLimit, tier, share);
	}

	@Override
	public abstract String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer);

	public int getCharge(ItemStack itemStack)
	{
		return ElectricItem.manager.getCharge(itemStack);
	}

	@Override
	public double getDamageAbsorptionRatio()
	{
		return 0.0D;
	}

	@Override
	public int getEnergyPerDamage()
	{
		return 0;
	}

	public abstract int getItemTier();

	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs, List itemList)
	{
		ItemStack itemStack = new ItemStack(this, 1);
		if (getChargedItemId(itemStack) == this.itemID)
		{
			ItemStack charged = new ItemStack(this, 1);
			ElectricItem.manager.charge(charged, Integer.MAX_VALUE, Integer.MAX_VALUE, true, false);
			itemList.add(charged);
		}
		if (getEmptyItemId(itemStack) == this.itemID) {
			itemList.add(new ItemStack(this, 1, this.getMaxDamage()));
		}
	}

	@Override
	public void onArmorTickUpdate(World world, EntityPlayer player, ItemStack itemStack)
	{
		if (player.inventory.armorInventory[2] != itemStack) {
			return;
		}
		NBTTagCompound nbtData = StackUtil.getOrCreateNbtData(itemStack);
		if(!nbtData.hasKey("jetEnabled"))
		{
			nbtData.setBoolean("jetEnabled", true);
		}
		if(nbtData.getBoolean("jetEnabled"))
		{
			boolean hoverMode = nbtData.getBoolean("hoverMode");
			byte toggleTimer = nbtData.getByte("toggleTimer");
			boolean jetpackUsed = false;
			boolean flag = ((!nbtData.getBoolean("flight")) || (nbtData.getBoolean("flight") && !nbtData.getBoolean("isFlyActive")));
			if (IC2.keyboard.isJumpKeyDown(player) && IC2.keyboard.isModeSwitchKeyDown(player) && toggleTimer == 0)
			{
				toggleTimer = 10;
				hoverMode = !hoverMode;
				if (IC2.platform.isSimulating() && flag)
				{
					nbtData.setBoolean("hoverMode", hoverMode);
					if (hoverMode) 
					{
						IC2.platform.messagePlayer(player, "Hover Mode enabled.");
					} 
					else 
					{
						IC2.platform.messagePlayer(player, "Hover Mode disabled.");
					}
				}
			}
			if (IC2.keyboard.isJumpKeyDown(player) || hoverMode && player.motionY < -0.3499999940395355D) 
			{
				if (flag)
				{
					jetpackUsed = this.useJetpack(player, hoverMode);
				}
			}
			if (IC2.platform.isSimulating() && toggleTimer > 0)
			{
				--toggleTimer;
				nbtData.setByte("toggleTimer", toggleTimer);
			}
			if (IC2.platform.isRendering() && player == IC2.platform.getPlayerInstance())
			{
				if (lastJetpackUsed != jetpackUsed)
				{
					if (jetpackUsed)
					{
						if (audioSource == null) 
						{
							audioSource = IC2.audioManager.createSource(player, PositionSpec.Backpack, "Tools/Jetpack/JetpackLoop.ogg", true, false, IC2.audioManager.defaultVolume);
						}
						if (audioSource != null) 
						{
							audioSource.play();
						}
					}
					else if (audioSource != null)
					{
						audioSource.remove();
						audioSource = null;
					}
					lastJetpackUsed = jetpackUsed;
				}
				if (audioSource != null) 
				{
					audioSource.updatePosition();
				}
			}
			if (jetpackUsed) 
			{
				player.inventoryContainer.detectAndSendChanges();
			}
		}
	}

	public void use(ItemStack itemStack, int amount)
	{
		ElectricItem.manager.discharge(itemStack, amount, 2147483647, true, false);
	}

	public boolean useJetpack(EntityPlayer player, boolean hoverMode)
	{
		ItemStack jetpack = player.inventory.armorInventory[2];

		if (getCharge(jetpack) == 0) return false;
		float power = 0.7F;
		float dropPercentage = 0.05F;
		if ((float)getCharge(jetpack) / (float)this.getDefaultMaxCharge() <= dropPercentage) {
			power *= (float)getCharge(jetpack) / ((float)this.getDefaultMaxCharge() * dropPercentage);
		}
		if (IC2.keyboard.isForwardKeyDown(player))
		{
			float retruster = 0.15F;
			if (hoverMode) retruster = 0.5F;
			retruster += 0.15F;

			float forwardpower = power * retruster * 2.0F;
			if (forwardpower > 0.0F) {
				player.moveFlying(0.0F, 0.4F * forwardpower, 0.02F);
			}
		}
		int maxFlightHeight = IC2.getWorldHeight(player.worldObj);

		double y = player.posY;
		if (y > maxFlightHeight - 25)
		{
			if (y > maxFlightHeight) y = maxFlightHeight;
			power = (float)((double)power * ((maxFlightHeight - y) / 25));
		}
		double prevmotion = player.motionY;
		player.motionY = Math.min(player.motionY + (double)(power * 0.2F), 0.6000000238418579D);
		if (hoverMode)
		{
			float maxHoverY = -0.1F;
			if (IC2.keyboard.isJumpKeyDown(player)) 
			{
				maxHoverY = 0.1F;
			}
			if (player.motionY > (double)maxHoverY)
			{
				player.motionY = maxHoverY;
				if (prevmotion > player.motionY) player.motionY = prevmotion;
			}
		}
		int consume = IC2CA.jetpackEUAmount;
		if (hoverMode) consume = (int)Math.floor(consume * 0.75);
		use(jetpack, consume);

		player.fallDistance = 0.0F;
		player.distanceWalkedModified = 0.0F;

		IC2.platform.resetPlayerInAirTime(player);

		return true;
	}

	@Override
	public void onFlykeyPressed(ItemStack is, EntityPlayer player)
	{
		NBTTagCompound nbt = StackUtil.getOrCreateNbtData(is);
		if(!nbt.hasKey("jetEnabled"))
		{
			nbt.setBoolean("jetEnabled", true);
		}
		nbt.setBoolean("jetEnabled", !nbt.getBoolean("jetEnabled"));
		if(IC2.platform.isSimulating())
		{
			if(nbt.getBoolean("jetEnabled"))
			{
				player.addChatMessage("Jetpack enabled.");
			}
			else
			{
				player.addChatMessage("Jetpack disabled.");
			}
		}
	}	
}
