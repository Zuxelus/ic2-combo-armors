package ic2ca.common.item;

import ic2.api.item.ElectricItem;
import ic2.api.item.IElectricItem;
import ic2ca.common.IC2CA;
import ic2ca.common.Util;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.world.World;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemDrill extends ItemTool implements IElectricItem
{
	public ItemDrill(int id, String name)
	{
		super(id, 1, EnumToolMaterial.STONE, new Block[0]);
		this.setMaxDamage(27);
		this.setMaxStackSize(1);
		this.setUnlocalizedName(name);    
		this.setCreativeTab(IC2CA.tabIC2CA);
		GameRegistry.registerItem(this, name);    
	}

	@Override
	public boolean canProvideEnergy(ItemStack is)
	{
		return false;
	}

	@Override
	public int getChargedItemId(ItemStack is)
	{
		return this.itemID;
	}

	@Override
	public int getEmptyItemId(ItemStack is)
	{
		return this.itemID;
	}

	@Override
	public boolean getIsRepairable(ItemStack var1, ItemStack var2)
	{
		return false;
	}

	@Override
	public int getItemEnchantability()
	{
		return 0;
	}

	@Override
	public int getMaxCharge(ItemStack is)
	{
		return 10000;
	}

	@Override
	@SideOnly(Side.CLIENT)  
	public void getSubItems(int var1, CreativeTabs var2, List itemList)
	{
		ItemStack charged = new ItemStack(this, 1);
		ElectricItem.manager.charge(charged, 2147483647, 2147483647, true, false);
		itemList.add(charged);
		itemList.add(new ItemStack(this, 1, getMaxDamage()));
	}

	@Override
	public int getTier(ItemStack is)
	{
		return 1;
	}

	@Override
	public int getTransferLimit(ItemStack is)
	{
		return 100;
	}

	@Override
	public boolean hitEntity(ItemStack par1ItemStack, EntityLivingBase par2EntityLiving, EntityLivingBase par3EntityLiving)
	{
		return true;
	}

	@Override
	public boolean isRepairable()
	{
		return false;
	}

	@Override
	public boolean onBlockDestroyed(ItemStack par1ItemStack, World world, int par3, int par4, int par5, int par6, EntityLivingBase par7EntityLiving)
	{
		return true;
	}

	@Override
	public void registerIcons(IconRegister ir)
	{
		this.itemIcon = Util.register(ir, this);
	}
}
