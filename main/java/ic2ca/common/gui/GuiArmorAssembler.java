package ic2ca.common.gui;

import ic2ca.common.container.ContainerArmorAssembler;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiArmorAssembler extends GuiContainer
{		
	public ContainerArmorAssembler container;	
	private static final ResourceLocation background = new ResourceLocation("ic2ca", "textures/gui/GuiArmorAssembler.png");

	public GuiArmorAssembler(ContainerArmorAssembler container1)
	{
		super(container1);

		this.container = container1;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(background);
		int x = (this.width - this.xSize) / 2;
		int y = (this.height - this.ySize) / 2;
		drawTexturedModalRect(x, y, 0, 0, this.xSize, this.ySize);	
		int chargeLevel = Math.round(this.container.tileEntity.getChargeLevel() * 14.0F);
		drawTexturedModalRect(x + 56, y + 36 + 14 - chargeLevel, 176, 14 - chargeLevel, 14, chargeLevel);    
		if (this.container.tileEntity.progress > 0)
		{
			int var7 = this.container.tileEntity.getProgressScaled();
			drawTexturedModalRect(x + 79, y + 34, 176, 14, var7 + 1, 16);
		}
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int par1, int par2)
	{	  
		this.fontRenderer.drawString(StatCollector.translateToLocal("info.armorassembler"), 8, 6, 4210752);
		this.fontRenderer.drawString(StatCollector.translateToLocal("info.inventory"), 8, this.ySize - 96 + 2, 4210752);
		this.fontRenderer.drawString(this.container.tileEntity.getEnergyString(), 56 - this.fontRenderer.getStringWidth(this.container.tileEntity.getEnergyString()), 39, 4210752);
		this.fontRenderer.drawString(this.container.tileEntity.getProgressString(), this.xSize - this.fontRenderer.getStringWidth(this.container.tileEntity.getProgressString()) - 8, this.ySize - 96 - 8, 4210752);
		this.fontRenderer.drawString(this.container.tileEntity.getTimeString(), this.xSize - this.fontRenderer.getStringWidth(this.container.tileEntity.getTimeString()) - 8, this.ySize - 96 + 2, 4210752);
	}
}
