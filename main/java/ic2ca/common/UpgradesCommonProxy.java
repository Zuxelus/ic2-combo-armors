package ic2ca.common;

import ic2.api.item.ElectricItem;
import ic2.core.util.StackUtil;
import ic2ca.common.item.ItemNBTHelper;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;

public class UpgradesCommonProxy
{
  public static boolean firstLoadServer(EntityPlayer player, ItemStack var1)
  {
    if (ItemNBTHelper.readFlyStatus(var1))
    {
      ItemNBTHelper.saveFlyStatus(var1, false);
      switchFlyModeServer(player, var1);
      CommonProxyic2ca.sendPacket(player, "setFlyStatus", 0);
    }
    if (ItemNBTHelper.readCloakStatus(var1))
    {
      ItemNBTHelper.saveCloakStatus(var1, false);
      switchCloakModeServer(player, var1);
      CommonProxyic2ca.sendPacket(player, "setFlyStatus", 0);
    }
    return true;
  }
  
  public static boolean onCloakTickServer(EntityPlayer player, ItemStack var1, float var2, float var3)
  {
    if (ServerTickHandleric2ca.checkLastCloakUndressed(player))
    {
      ItemNBTHelper.saveCloakStatus(var1, false);
      ServerTickHandleric2ca.lastCloakUndressed.put(player, Boolean.valueOf(false));
    }
    if (ItemNBTHelper.readCloakStatus(var1))
    {
      int var5 = ItemNBTHelper.getCharge(var1);
      if (player.isDead && (!player.worldObj.getGameRules().getGameRuleBooleanValue("keepInventory")))
      {
        NBTTagCompound helmetnbt = ServerTickHandleric2ca.getHelmetStored(player);
        if (helmetnbt != null && helmetnbt.getBoolean("has"))
        {
          int id = helmetnbt.getInteger("id");
          int damage = helmetnbt.getInteger("damage");
          int size = helmetnbt.getInteger("size");
          ItemStack is = new ItemStack(id, size, damage);
          if (helmetnbt.getCompoundTag("nbt") != null)
          {
            NBTTagCompound nbt = helmetnbt.getCompoundTag("nbt");
            is.setTagCompound(nbt);
          }
          EntityItem entityitem = new EntityItem(player.worldObj, player.posX + 0.5D, player.posY + 0.5D, player.posZ + 0.5D, is);
          entityitem.delayBeforeCanPickup = 10;
          player.worldObj.spawnEntityInWorld(entityitem);
          helmetnbt.removeTag("id");
          helmetnbt.removeTag("damage");
          helmetnbt.removeTag("size");
          helmetnbt.removeTag("nbt");
          helmetnbt.removeTag("has");
        }
        NBTTagCompound pantsnbt = ServerTickHandleric2ca.getPantsStored(player);
        if ((pantsnbt != null) && (pantsnbt.getBoolean("has")))
        {
          int id = pantsnbt.getInteger("id");
          int damage = pantsnbt.getInteger("damage");
          int size = pantsnbt.getInteger("size");
          ItemStack is = new ItemStack(id, size, damage);
          if (pantsnbt.getCompoundTag("nbt") != null)
          {
            NBTTagCompound nbt = pantsnbt.getCompoundTag("nbt");
            is.setTagCompound(nbt);
          }
          EntityItem entityitem = new EntityItem(player.worldObj, player.posX + 0.5D, player.posY + 0.5D, player.posZ + 0.5D, is);
          entityitem.delayBeforeCanPickup = 10;
          player.worldObj.spawnEntityInWorld(entityitem);
          pantsnbt.removeTag("id");
          pantsnbt.removeTag("damage");
          pantsnbt.removeTag("size");
          pantsnbt.removeTag("nbt");
          pantsnbt.removeTag("has");
        }
        NBTTagCompound bootsnbt = ServerTickHandleric2ca.getBootsStored(player);
        if ((bootsnbt != null) && (bootsnbt.getBoolean("has")))
        {
          int id = bootsnbt.getInteger("id");
          int damage = bootsnbt.getInteger("damage");
          int size = bootsnbt.getInteger("size");
          ItemStack is = new ItemStack(id, size, damage);
          if (bootsnbt.getCompoundTag("nbt") != null)
          {
            NBTTagCompound nbt = bootsnbt.getCompoundTag("nbt");
            is.setTagCompound(nbt);
          }
          EntityItem entityitem = new EntityItem(player.worldObj, player.posX + 0.5D, player.posY + 0.5D, player.posZ + 0.5D, is);
          entityitem.delayBeforeCanPickup = 10;
          player.worldObj.spawnEntityInWorld(entityitem);
          bootsnbt.removeTag("id");
          bootsnbt.removeTag("damage");
          bootsnbt.removeTag("size");
          bootsnbt.removeTag("nbt");
          bootsnbt.removeTag("has");
        }
        NBTTagCompound chestnbt = ServerTickHandleric2ca.getChestStored(player);
        if ((chestnbt != null) && (chestnbt.getBoolean("has")))
        {
          int id = chestnbt.getInteger("id");
          int damage = chestnbt.getInteger("damage");
          int size = chestnbt.getInteger("size");
          ItemStack is = new ItemStack(id, size, damage);
          if (chestnbt.getCompoundTag("nbt") != null)
          {
            NBTTagCompound nbt = chestnbt.getCompoundTag("nbt");
            is.setTagCompound(nbt);
          }
          EntityItem entityitem = new EntityItem(player.worldObj, player.posX + 0.5D, player.posY + 0.5D, player.posZ + 0.5D, is);
          entityitem.delayBeforeCanPickup = 10;
          player.worldObj.spawnEntityInWorld(entityitem);
          switchCloakModeServer(player, is);
          chestnbt.removeTag("id");
          chestnbt.removeTag("damage");
          chestnbt.removeTag("size");
          chestnbt.removeTag("nbt");
          chestnbt.removeTag("has");
        }
      }
      else if ((player.isDead) && (player.worldObj.getGameRules().getGameRuleBooleanValue("keepInventory")))
      {
        NBTTagCompound chestnbt = ServerTickHandleric2ca.getChestStored(player);
        if ((chestnbt != null) && (chestnbt.getBoolean("has")))
        {
          int id = chestnbt.getInteger("id");
          int damage = chestnbt.getInteger("damage");
          int size = chestnbt.getInteger("size");
          ItemStack is = new ItemStack(id, size, damage);
          if (chestnbt.getCompoundTag("nbt") != null)
          {
            NBTTagCompound nbt = chestnbt.getCompoundTag("nbt");
            is.setTagCompound(nbt);
          }
          switchCloakModeServer(player, is);
        }
      }
      if (!player.capabilities.isCreativeMode) {
        if (var5 < 10)
        {
          CommonProxyic2ca.sendPlayerMessage(player, "Out of energy!");
          switchCloakModeServer(player, var1);
        }
        else
        {
          ElectricItem.manager.discharge(var1, 10, 4, true, false);
        }
      }
    }
    return true;
  }
  
  public static boolean onFlyTickServer(EntityPlayer player, ItemStack var1, float var2, float var3)
  {
    if (ServerTickHandleric2ca.checkLastFlyUndressed(player))
    {
      ItemNBTHelper.saveFlyStatus(var1, false);
      ServerTickHandleric2ca.lastFlyUndressed.put(player, Boolean.valueOf(false));
    }
    if (ItemNBTHelper.readFlyStatus(var1))
    {
      int var5 = ItemNBTHelper.getCharge(var1);
      if (!player.capabilities.isCreativeMode) {
        if (var5 < 10)
        {
          CommonProxyic2ca.sendPlayerMessage(player, "Out of energy!");
          switchFlyModeServer(player, var1);
        }
        else if (player.capabilities.isFlying)
        {
          ElectricItem.manager.discharge(var1, IC2CA.turbineEUAmount, 4, true, false);
        }
      }
      player.fallDistance = 0.0F;
    }
    return true;
  }
  
  public static void overcharge(EntityPlayer player, ItemStack var1, double x, double y, double z)
  {
    int charge = ItemNBTHelper.getCharge(var1);
    int maxcharge = StackUtil.getOrCreateNbtData(var1).getInteger("maxCharge");
    int overchargenumber = maxcharge / 10;
    int boltnumber = overchargenumber / 10000;
    if (boltnumber < 1)
    {
      CommonProxyic2ca.sendPlayerMessage(player, "This " + var1.getDisplayName() + " does not have a high enough max charge to discharge.");
    }
    else
    {
      if (boltnumber >= 10) {
        boltnumber = 10;
      }
      if (charge >= boltnumber * 10000)
      {
        for (int i = 1; i <= boltnumber; i++)
        {
          EntityLightningBolt elb = new EntityLightningBolt(player.worldObj, x, y, z);
          player.worldObj.spawnEntityInWorld(elb);
          ElectricItem.manager.discharge(var1, 10000, 4, true, false);
        }
        CommonProxyic2ca.sendPlayerMessage(player, "Discharged " + boltnumber * 10000 + " EU.");
      }
      else
      {
        CommonProxyic2ca.sendPlayerMessage(player, "Not enough energy to discharge!");
      }
    }
  }
  
  public static boolean switchCloakModeServer(EntityPlayer player, ItemStack var1)
  {
    if (ItemNBTHelper.readCloakStatus(var1))
    {
      player.removePotionEffect(Potion.invisibility.id);
      
      NBTTagCompound helmetnbt = ServerTickHandleric2ca.getHelmetStored(player);
      if ((helmetnbt != null) && (helmetnbt.getBoolean("has")))
      {
        int id = helmetnbt.getInteger("id");
        int damage = helmetnbt.getInteger("damage");
        int size = helmetnbt.getInteger("size");
        ItemStack is = new ItemStack(id, size, damage);
        if (helmetnbt.getCompoundTag("nbt") != null)
        {
          NBTTagCompound nbt = helmetnbt.getCompoundTag("nbt");
          is.setTagCompound(nbt);
        }
        player.inventory.armorInventory[3] = is;
        helmetnbt.removeTag("id");
        helmetnbt.removeTag("damage");
        helmetnbt.removeTag("size");
        helmetnbt.removeTag("nbt");
        helmetnbt.removeTag("has");
      }
      NBTTagCompound chestnbt = ServerTickHandleric2ca.getChestStored(player);
      if ((chestnbt != null) && (chestnbt.getBoolean("has")))
      {
        int id = chestnbt.getInteger("id");
        int damage = chestnbt.getInteger("damage");
        int size = chestnbt.getInteger("size");
        ItemStack is = new ItemStack(id, size, damage);
        if (chestnbt.getCompoundTag("nbt") != null)
        {
          NBTTagCompound nbt = chestnbt.getCompoundTag("nbt");
          is.setTagCompound(nbt);
        }
        player.inventory.armorInventory[2] = is;
        chestnbt.removeTag("id");
        chestnbt.removeTag("damage");
        chestnbt.removeTag("size");
        chestnbt.removeTag("nbt");
        chestnbt.removeTag("has");
      }
      NBTTagCompound pantsnbt = ServerTickHandleric2ca.getPantsStored(player);
      if ((pantsnbt != null) && (pantsnbt.getBoolean("has")))
      {
        int id = pantsnbt.getInteger("id");
        int damage = pantsnbt.getInteger("damage");
        int size = pantsnbt.getInteger("size");
        ItemStack is = new ItemStack(id, size, damage);
        if (pantsnbt.getCompoundTag("nbt") != null)
        {
          NBTTagCompound nbt = pantsnbt.getCompoundTag("nbt");
          is.setTagCompound(nbt);
        }
        player.inventory.armorInventory[1] = is;
        pantsnbt.removeTag("id");
        pantsnbt.removeTag("damage");
        pantsnbt.removeTag("size");
        pantsnbt.removeTag("nbt");
        pantsnbt.removeTag("has");
      }
      NBTTagCompound bootsnbt = ServerTickHandleric2ca.getBootsStored(player);
      if ((bootsnbt != null) && (bootsnbt.getBoolean("has")))
      {
        int id = bootsnbt.getInteger("id");
        int damage = bootsnbt.getInteger("damage");
        int size = bootsnbt.getInteger("size");
        ItemStack is = new ItemStack(id, size, damage);
        if (bootsnbt.getCompoundTag("nbt") != null)
        {
          NBTTagCompound nbt = bootsnbt.getCompoundTag("nbt");
          is.setTagCompound(nbt);
        }
        player.inventory.armorInventory[0] = is;
        bootsnbt.removeTag("id");
        bootsnbt.removeTag("damage");
        bootsnbt.removeTag("size");
        bootsnbt.removeTag("nbt");
        bootsnbt.removeTag("has");
      }
      CommonProxyic2ca.sendPlayerMessage(player, "Cloaking engine disabled.");
      ServerTickHandleric2ca.isCloakActiveByMod.put(player, Boolean.valueOf(false));
      ItemNBTHelper.saveCloakStatus(var1, false);
    }
    else
    {
      int var2 = ItemNBTHelper.getCharge(var1);
      if ((var2 < 10) && (!player.capabilities.isCreativeMode))
      {
        CommonProxyic2ca.sendPlayerMessage(player, "Not enough energy to enable cloaking engine!");
      }
      else
      {
        CommonProxyic2ca.sendPlayerMessage(player, "Cloaking engine enabled.");
        player.addPotionEffect(new PotionEffect(Potion.invisibility.id, 19, 1));
        if (player.inventory.armorInventory[3] != null)
        {
          NBTTagCompound helmetnbt = new NBTTagCompound();
          helmetnbt.setBoolean("has", true);
          helmetnbt.setInteger("id", player.inventory.armorInventory[3].getItem().itemID);
          helmetnbt.setInteger("damage", player.inventory.armorInventory[3].getItemDamage());
          helmetnbt.setInteger("size", player.inventory.armorInventory[3].stackSize);
          if (player.inventory.armorInventory[3].getTagCompound() != null) {
            helmetnbt.setCompoundTag("nbt", StackUtil.getOrCreateNbtData(player.inventory.armorInventory[3]));
          }
          ServerTickHandleric2ca.helmetStored.put(player, helmetnbt);
          player.inventory.armorInventory[3] = null;
        }
        if (player.inventory.armorInventory[2] != null)
        {
          NBTTagCompound chestnbt = new NBTTagCompound();
          chestnbt.setBoolean("has", true);
          chestnbt.setInteger("id", player.inventory.armorInventory[2].getItem().itemID);
          chestnbt.setInteger("damage", player.inventory.armorInventory[2].getItemDamage());
          chestnbt.setInteger("size", player.inventory.armorInventory[2].stackSize);
          if (player.inventory.armorInventory[2].getTagCompound() != null) {
            chestnbt.setCompoundTag("nbt", StackUtil.getOrCreateNbtData(player.inventory.armorInventory[2]));
          }
          ServerTickHandleric2ca.chestStored.put(player, chestnbt);
          player.inventory.armorInventory[2] = null;
        }
        if (player.inventory.armorInventory[1] != null)
        {
          NBTTagCompound pantsnbt = new NBTTagCompound();
          pantsnbt.setBoolean("has", true);
          pantsnbt.setInteger("id", player.inventory.armorInventory[1].getItem().itemID);
          pantsnbt.setInteger("damage", player.inventory.armorInventory[1].getItemDamage());
          pantsnbt.setInteger("size", player.inventory.armorInventory[1].stackSize);
          if (player.inventory.armorInventory[1].getTagCompound() != null) {
            pantsnbt.setCompoundTag("nbt", StackUtil.getOrCreateNbtData(player.inventory.armorInventory[1]));
          }
          ServerTickHandleric2ca.pantsStored.put(player, pantsnbt);
          player.inventory.armorInventory[1] = null;
        }
        if (player.inventory.armorInventory[0] != null)
        {
          NBTTagCompound bootsnbt = new NBTTagCompound();
          bootsnbt.setBoolean("has", true);
          bootsnbt.setInteger("id", player.inventory.armorInventory[0].getItem().itemID);
          bootsnbt.setInteger("damage", player.inventory.armorInventory[0].getItemDamage());
          bootsnbt.setInteger("size", player.inventory.armorInventory[0].stackSize);
          if (player.inventory.armorInventory[0].getTagCompound() != null) {
            bootsnbt.setCompoundTag("nbt", StackUtil.getOrCreateNbtData(player.inventory.armorInventory[0]));
          }
          ServerTickHandleric2ca.bootsStored.put(player, bootsnbt);
          player.inventory.armorInventory[0] = null;
        }
        ServerTickHandleric2ca.isCloakActiveByMod.put(player, Boolean.valueOf(true));
        ItemNBTHelper.saveCloakStatus(var1, true);
      }
    }
    return true;
  }
  
  public static boolean switchFlyModeServer(EntityPlayer player, ItemStack var1)
  {
    if (ItemNBTHelper.readFlyStatus(var1))
    {
      if (!player.capabilities.isCreativeMode)
      {
        player.capabilities.allowFlying = false;
        player.capabilities.isFlying = false;
      }
      CommonProxyic2ca.sendPlayerMessage(player, "Flight Turbine disabled.");
      ServerTickHandleric2ca.isFlyActiveByMod.put(player, Boolean.valueOf(false));
      ItemNBTHelper.saveFlyStatus(var1, false);
    }
    else
    {
      int var2 = ItemNBTHelper.getCharge(var1);
      if ((var2 < 10) && (!player.capabilities.isCreativeMode))
      {
        CommonProxyic2ca.sendPlayerMessage(player, "Not enough energy to enable flight mode!");
      }
      else
      {
        CommonProxyic2ca.sendPlayerMessage(player, "Flight Turbine enabled.");
        player.capabilities.allowFlying = true;
        player.capabilities.isFlying = true;
        ServerTickHandleric2ca.isFlyActiveByMod.put(player, Boolean.valueOf(true));
        ItemNBTHelper.saveFlyStatus(var1, true);
      }
    }
    return true;
  }
}
