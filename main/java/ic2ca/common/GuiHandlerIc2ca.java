package ic2ca.common;

import ic2ca.common.container.ContainerArmorAssembler;
import ic2ca.common.gui.GuiArmorAssembler;
import ic2ca.common.tileentity.TileEntityArmorAssembler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import cpw.mods.fml.common.network.IGuiHandler;

public class GuiHandlerIc2ca implements IGuiHandler {

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		TileEntity tileEntity = (TileEntity)world.getBlockTileEntity(x, y, z);
		if (tileEntity != null && tileEntity instanceof TileEntityArmorAssembler)
		{
			return new GuiArmorAssembler(new ContainerArmorAssembler(player, (TileEntityArmorAssembler) tileEntity));
		}
		return null;
	}

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		TileEntity tileEntity = (TileEntity)world.getBlockTileEntity(x, y, z);
		if (tileEntity != null && tileEntity instanceof TileEntityArmorAssembler)
		{
			return new ContainerArmorAssembler(player, (TileEntityArmorAssembler) tileEntity);
		}
		return null;
	}
	
}
