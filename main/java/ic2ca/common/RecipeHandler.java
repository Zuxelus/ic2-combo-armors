package ic2ca.common;

import ic2.api.item.Items;
import ic2.api.recipe.Recipes;
import ic2.core.Ic2Items;
import ic2.core.util.StackUtil;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class RecipeHandler
{
  private static final RecipeHandler instance = new RecipeHandler();
  
  public static RecipeHandler instance()
  {
    return instance;
  }
  
  public void addChestpieceRecipes(ItemStack upgrade)
  {
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoNanoChest, Ic2caItems.exoNanoChest, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoQuantumChest, Ic2caItems.exoQuantumChest, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoBatpack, Ic2caItems.exoBatpack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoAdvBatpack, Ic2caItems.exoAdvBatpack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoEnergypack, Ic2caItems.exoEnergypack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoJetpack, Ic2caItems.exoJetpack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoJetpack, Ic2caItems.nanoJetpack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoBatpack, Ic2caItems.nanoBatpack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoAdvBatpack, Ic2caItems.nanoAdvBatpack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoEnergypack, Ic2caItems.nanoEnergypack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoUltimate, Ic2caItems.nanoUltimate, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumBatpack, Ic2caItems.quantumBatpack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumAdvBatpack, Ic2caItems.quantumAdvBatpack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumEnergypack, Ic2caItems.quantumEnergypack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumJetpack, Ic2caItems.quantumJetpack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumUltimate, Ic2caItems.quantumUltimate, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.jetpackBatpack, Ic2caItems.jetpackBatpack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.jetpackAdvBatpack, Ic2caItems.jetpackAdvBatpack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.jetpackEnergypack, Ic2caItems.jetpackEnergypack, upgrade);
  }
  
  public void addComboRecipes()
  {
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.solarNanoHelm, Ic2caItems.exoNanoHelm, Ic2Items.solarHelmet);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.solarNanoHelm, Ic2caItems.exoSolar, Ic2Items.nanoHelmet);

	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.solarQuantumHelm, Ic2caItems.exoQuantumHelm, Ic2Items.solarHelmet);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.solarQuantumHelm, Ic2caItems.exoSolar, Ic2Items.quantumHelmet);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.solarQuantumHelm, Ic2caItems.solarNanoHelm, Ic2Items.quantumHelmet);

	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoBatpack, Ic2caItems.exoNanoChest, Ic2Items.batPack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoBatpack, Ic2caItems.exoBatpack, Ic2Items.nanoBodyarmor);

	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoAdvBatpack, Ic2caItems.exoNanoChest, Ic2Items.advbatPack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoAdvBatpack, Ic2caItems.exoAdvBatpack, Ic2Items.nanoBodyarmor);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoAdvBatpack, Ic2caItems.nanoBatpack, Ic2Items.advbatPack);

	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoEnergypack, Ic2caItems.exoNanoChest, Ic2Items.energyPack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoEnergypack, Ic2caItems.exoEnergypack, Ic2Items.nanoBodyarmor);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoEnergypack, Ic2caItems.nanoAdvBatpack, Ic2Items.energyPack);	  
	  
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoJetpack, Ic2caItems.exoNanoChest, Ic2Items.electricJetpack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoJetpack, Ic2caItems.exoJetpack, Ic2Items.nanoBodyarmor);

	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoUltimate, Ic2Items.nanoBodyarmor, Ic2caItems.jetpackEnergypack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoUltimate, Ic2Items.electricJetpack, Ic2caItems.nanoEnergypack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoUltimate, Ic2Items.energyPack, Ic2caItems.nanoJetpack);

	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumBatpack, Ic2Items.batPack, Ic2caItems.exoQuantumChest);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumBatpack, Ic2Items.quantumBodyarmor, Ic2caItems.exoBatpack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumBatpack, Ic2caItems.nanoBatpack, Ic2Items.quantumBodyarmor);

	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumAdvBatpack, Ic2Items.advbatPack, Ic2caItems.exoQuantumChest);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumAdvBatpack, Ic2Items.quantumBodyarmor, Ic2caItems.exoAdvBatpack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumAdvBatpack, Ic2caItems.nanoAdvBatpack, Ic2Items.quantumBodyarmor);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumAdvBatpack, Ic2caItems.quantumBatpack, Ic2Items.advbatPack);
	  
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumEnergypack, Ic2Items.energyPack, Ic2caItems.exoQuantumChest);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumEnergypack, Ic2Items.quantumBodyarmor, Ic2caItems.exoEnergypack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumEnergypack, Ic2caItems.nanoEnergypack, Ic2Items.quantumBodyarmor);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumEnergypack, Ic2caItems.quantumAdvBatpack, Ic2Items.energyPack);	  

	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumJetpack, Ic2Items.electricJetpack, Ic2caItems.exoQuantumChest);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumJetpack, Ic2Items.quantumBodyarmor, Ic2caItems.exoJetpack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumJetpack, Ic2caItems.nanoJetpack, Ic2Items.quantumBodyarmor);

	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumUltimate, Ic2Items.electricJetpack, Ic2caItems.quantumEnergypack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumUltimate, Ic2Items.energyPack, Ic2caItems.quantumJetpack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumUltimate, Ic2Items.quantumBodyarmor, Ic2caItems.jetpackEnergypack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumUltimate, Ic2caItems.nanoUltimate, Ic2Items.quantumBodyarmor);

	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.jetpackBatpack, Ic2Items.electricJetpack, Ic2caItems.exoBatpack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.jetpackBatpack, Ic2caItems.exoJetpack, Ic2Items.batPack);

	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.jetpackAdvBatpack, Ic2Items.electricJetpack, Ic2caItems.exoAdvBatpack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.jetpackAdvBatpack, Ic2caItems.exoJetpack, Ic2Items.advbatPack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.jetpackAdvBatpack, Ic2caItems.jetpackBatpack, Ic2Items.advbatPack);
	  
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.jetpackEnergypack, Ic2Items.electricJetpack, Ic2caItems.exoEnergypack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.jetpackEnergypack, Ic2caItems.exoJetpack, Ic2Items.energyPack);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.jetpackEnergypack, Ic2caItems.jetpackAdvBatpack, Ic2Items.energyPack);

	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoStatic, Ic2Items.staticBoots, Ic2caItems.exoNanoBoots);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoStatic, Ic2Items.nanoBoots, Ic2caItems.exoStatic);

	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumStatic, Ic2Items.staticBoots, Ic2caItems.exoQuantumBoots);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumStatic, Ic2Items.quantumBoots, Ic2caItems.exoStatic);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumStatic, Ic2caItems.nanoStatic, Ic2Items.quantumBoots);
  }
  
  public void addCraftingRecipes()
  {
    Recipes.advRecipes.addRecipe(Ic2caItems.drillBit, new Object[] { "RRA", Character.valueOf('R'), "plateIron", Character.valueOf('A'), Ic2Items.advancedAlloy });
    Recipes.advRecipes.addRecipe(Ic2caItems.drill, new Object[] { "BRE", "  P", "  R", Character.valueOf('R'), "plateIron", Character.valueOf('B'), Ic2caItems.drillBit, Character.valueOf('E'), Ic2Items.electronicCircuit, Character.valueOf('P'), StackUtil.copyWithWildCard(Ic2Items.reBattery) });
    Recipes.advRecipes.addRecipe(Ic2caItems.drill, new Object[] { "BRE", "  P", "  R", Character.valueOf('R'), "plateIron", Character.valueOf('B'), Ic2caItems.drillBit, Character.valueOf('E'), Ic2Items.electronicCircuit, Character.valueOf('P'), StackUtil.copyWithWildCard(Ic2Items.chargedReBattery) });
    Recipes.advRecipes.addRecipe(Ic2caItems.armorAssembler, new Object[] { "GDG", "ALA", "GCG", Character.valueOf('G'), Item.glowstone, Character.valueOf('D'), StackUtil.copyWithWildCard(Ic2caItems.drill), Character.valueOf('C'), Ic2Items.advancedCircuit, Character.valueOf('L'), StackUtil.copyWithWildCard(Ic2Items.lapotronCrystal), Character.valueOf('A'), Ic2Items.advancedMachine });
    if (IC2CA.craftNanoBow) {
      Recipes.advRecipes.addRecipe(Ic2caItems.nanoBow, new Object[] { " CS", "E S", " CS", Character.valueOf('G'), Item.glowstone, Character.valueOf('C'), Ic2Items.carbonPlate, Character.valueOf('S'), Ic2Items.glassFiberCableItem, Character.valueOf('A'), Ic2Items.advancedAlloy, Character.valueOf('E'), StackUtil.copyWithWildCard(Ic2Items.energyCrystal) });
    }
    Recipes.advRecipes.addRecipe(StackUtil.copyWithSize(Ic2caItems.exoModule, 4), new Object[] { "RRR", "RCR", "RRR", Character.valueOf('R'), "plateIron", Character.valueOf('C'), Ic2Items.electronicCircuit });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoNanoHelm,new Object[] { "EEE", "EHE", Character.valueOf('E'), Ic2caItems.exoModule, Character.valueOf('H'), StackUtil.copyWithWildCard(Ic2Items.nanoHelmet) });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoNanoChest,new Object[] { "ENE", "EEE", "EEE", Character.valueOf('E'), Ic2caItems.exoModule, Character.valueOf('N'), StackUtil.copyWithWildCard(Ic2Items.nanoBodyarmor) });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoNanoLegs,new Object[] { "EEE", "ENE", "E E", Character.valueOf('E'), Ic2caItems.exoModule, Character.valueOf('N'), StackUtil.copyWithWildCard(Ic2Items.nanoLeggings) });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoNanoBoots,new Object[] { "ENE", "E E", Character.valueOf('E'), Ic2caItems.exoModule, Character.valueOf('N'), StackUtil.copyWithWildCard(Ic2Items.nanoBoots) });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoQuantumHelm,new Object[] { "EEE", "EHE", Character.valueOf('E'), Ic2caItems.exoModule, Character.valueOf('H'), StackUtil.copyWithWildCard(Ic2Items.quantumHelmet) });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoQuantumChest,new Object[] { "ENE", "EEE", "EEE", Character.valueOf('E'), Ic2caItems.exoModule, Character.valueOf('N'), StackUtil.copyWithWildCard(Ic2Items.quantumBodyarmor) });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoQuantumLegs,new Object[] { "EEE", "ENE", "E E", Character.valueOf('E'), Ic2caItems.exoModule, Character.valueOf('N'), StackUtil.copyWithWildCard(Ic2Items.quantumLeggings) });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoQuantumBoots,new Object[] { "ENE", "E E", Character.valueOf('E'), Ic2caItems.exoModule, Character.valueOf('N'), StackUtil.copyWithWildCard(Ic2Items.quantumBoots) });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoQuantumHelm,new Object[] { " n ", "ILI", "CGC", Character.valueOf('n'), Ic2caItems.exoNanoHelm, Character.valueOf('I'), Ic2Items.iridiumPlate, Character.valueOf('L'), StackUtil.copyWithWildCard(Ic2Items.lapotronCrystal), Character.valueOf('G'), Ic2Items.reinforcedGlass, Character.valueOf('C'), Ic2Items.advancedCircuit });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoQuantumChest,new Object[] { "AnA", "ILI", "IAI", Character.valueOf('n'), Ic2caItems.exoNanoChest, Character.valueOf('I'), Ic2Items.iridiumPlate, Character.valueOf('L'), StackUtil.copyWithWildCard(Ic2Items.lapotronCrystal), Character.valueOf('A'), Ic2Items.advancedAlloy });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoQuantumLegs,new Object[] { "MLM", "InI", "G G", Character.valueOf('n'), Ic2caItems.exoNanoLegs, Character.valueOf('I'), Ic2Items.iridiumPlate, Character.valueOf('L'), StackUtil.copyWithWildCard(Ic2Items.lapotronCrystal), Character.valueOf('G'), Item.glowstone, Character.valueOf('M'), Ic2Items.machine });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoQuantumBoots,new Object[] { "InI", "RLR", Character.valueOf('n'), Ic2caItems.exoNanoBoots, Character.valueOf('I'), Ic2Items.iridiumPlate, Character.valueOf('L'), StackUtil.copyWithWildCard(Ic2Items.lapotronCrystal), Character.valueOf('R'), Ic2Items.hazmatBoots });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoSolar,new Object[] { "EEE", "ESE", Character.valueOf('E'), Ic2caItems.exoModule, Character.valueOf('S'), Ic2Items.solarHelmet });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoStatic,new Object[] { "ESE", "E E", Character.valueOf('E'), Ic2caItems.exoModule, Character.valueOf('S'), Ic2Items.staticBoots });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoJetpack,new Object[] { "EJE", "EEE", "EEE", Character.valueOf('E'), Ic2caItems.exoModule, Character.valueOf('J'), StackUtil.copyWithWildCard(Ic2Items.electricJetpack) });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoBatpack,new Object[] { "EBE", "EEE", "EEE", Character.valueOf('E'), Ic2caItems.exoModule, Character.valueOf('B'), StackUtil.copyWithWildCard(Ic2Items.batPack) });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoAdvBatpack,new Object[] { "EAE", "EEE", "EEE", Character.valueOf('E'), Ic2caItems.exoModule, Character.valueOf('A'), StackUtil.copyWithWildCard(Ic2Items.advbatPack) });
    Recipes.advRecipes.addRecipe(Ic2caItems.exoEnergypack,new Object[] { "ELE", "EEE", "EEE", Character.valueOf('E'), Ic2caItems.exoModule, Character.valueOf('L'), StackUtil.copyWithWildCard(Ic2Items.energyPack) });    
    Recipes.advRecipes.addRecipe(Ic2caItems.speedBooster,new Object[] { "IOI", "OAO", "IOI", Character.valueOf('I'), Ic2Items.iridiumPlate, Character.valueOf('O'), Ic2Items.overclockerUpgrade, Character.valueOf('A'), Ic2Items.advancedCircuit });
  }
  
  public void addElectricRecipes(ItemStack upgrade)
  {
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoBow, upgrade, Ic2caItems.nanoBow);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.solarNanoHelm, upgrade, Ic2caItems.solarNanoHelm);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.solarQuantumHelm, upgrade, Ic2caItems.solarQuantumHelm);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoBatpack, upgrade, Ic2caItems.nanoBatpack);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoAdvBatpack, upgrade, Ic2caItems.nanoAdvBatpack);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoEnergypack, upgrade, Ic2caItems.nanoEnergypack);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoJetpack, upgrade, Ic2caItems.nanoJetpack);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoUltimate, upgrade, Ic2caItems.nanoUltimate);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumBatpack, upgrade, Ic2caItems.quantumBatpack);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumAdvBatpack, upgrade, Ic2caItems.quantumAdvBatpack);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumEnergypack, upgrade, Ic2caItems.quantumEnergypack);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumJetpack, upgrade, Ic2caItems.quantumJetpack);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumUltimate, upgrade, Ic2caItems.quantumUltimate);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.jetpackBatpack, upgrade, Ic2caItems.jetpackBatpack);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.jetpackAdvBatpack, upgrade, Ic2caItems.jetpackAdvBatpack);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.jetpackEnergypack, upgrade, Ic2caItems.jetpackEnergypack);    
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoStatic, upgrade, Ic2caItems.nanoStatic);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumStatic, upgrade, Ic2caItems.quantumStatic);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoNanoHelm, upgrade, Ic2caItems.exoNanoHelm);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoNanoChest, upgrade, Ic2caItems.exoNanoChest);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoNanoLegs, upgrade, Ic2caItems.exoNanoLegs);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoNanoBoots, upgrade, Ic2caItems.exoNanoBoots);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoQuantumHelm, upgrade, Ic2caItems.exoQuantumHelm);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoQuantumChest, upgrade, Ic2caItems.exoQuantumChest);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoQuantumLegs, upgrade, Ic2caItems.exoQuantumLegs);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoQuantumBoots, upgrade, Ic2caItems.exoQuantumBoots);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoJetpack, upgrade, Ic2caItems.exoJetpack);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoBatpack, upgrade, Ic2caItems.exoBatpack);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoAdvBatpack, upgrade, Ic2caItems.exoAdvBatpack);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoEnergypack, upgrade, Ic2caItems.exoEnergypack);
  }
  
  public void addJetpackRecipes(ItemStack upgrade)
  {
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoJetpack, Ic2caItems.nanoJetpack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoUltimate, Ic2caItems.nanoUltimate, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumJetpack, Ic2caItems.quantumJetpack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumUltimate, Ic2caItems.quantumUltimate, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.jetpackBatpack, Ic2caItems.jetpackBatpack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.jetpackAdvBatpack, Ic2caItems.jetpackAdvBatpack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.jetpackEnergypack, Ic2caItems.jetpackEnergypack, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoJetpack, Ic2caItems.exoJetpack, upgrade);
  }
  
  public void addSolarRecipes(ItemStack upgrade)
  {
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.solarNanoHelm, Ic2caItems.solarNanoHelm, upgrade);
	  ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.solarQuantumHelm, Ic2caItems.solarQuantumHelm, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoSolar, Ic2caItems.exoSolar, upgrade);
  }
  
  public void addStaticRecipes(ItemStack upgrade)
  {
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.nanoStatic, Ic2caItems.nanoStatic, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.quantumStatic, Ic2caItems.quantumStatic, upgrade);
    ArmorAssemblerRecipes.addAssemblyRecipe(Ic2caItems.exoStatic, Ic2caItems.exoStatic, upgrade);
  }
}
