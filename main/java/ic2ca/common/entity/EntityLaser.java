package ic2ca.common.entity;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityLaser extends EntityThrowable
{
	public EntityLaser(World par1World)
	{
		super(par1World);
	}

	public EntityLaser(World world, double par2, double par4, double par6)
	{
		super(world, par2, par4, par6);
	}

	public EntityLaser(World world, EntityLiving par2EntityLiving)
	{
		super(world, par2EntityLiving);
	}

    @Override
	protected void onImpact(MovingObjectPosition par1MovingObjectPosition)
	{
		if (par1MovingObjectPosition.entityHit != null)
		{
			par1MovingObjectPosition.entityHit.attackEntityFrom(DamageSource.causeThrownDamage(this, getThrower()), 1);
			par1MovingObjectPosition.entityHit.setFire(100);
		}
		for (int i = 0; i < 8; ++i) 
		{
			this.worldObj.spawnParticle("largesmoke", this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D);
		}
		if (!this.worldObj.isRemote) 
		{
			setDead();
		}
	}
}
