package ic2ca.common;

import ic2.api.item.ElectricItem;
import ic2.api.item.IElectricItem;
import ic2.api.item.Items;
import ic2.api.recipe.Recipes;
import ic2.core.IC2;
import ic2.core.Ic2Items;
import ic2.core.item.armor.ItemArmorQuantumSuit;
import ic2.core.util.StackUtil;
import ic2ca.client.ClientPacketHandleric2ca;
import ic2ca.client.ClientTickHandleric2ca;
import ic2ca.common.block.BlockArmorAssembler;
import ic2ca.common.block.BlockArmorCharger;
import ic2ca.common.entity.EntityLaser;
import ic2ca.common.entity.EntityTechArrow;
import ic2ca.common.item.EnumUpgradeType;
import ic2ca.common.item.ItemAssemblerUpgrade;
import ic2ca.common.item.ItemDrill;
import ic2ca.common.item.ItemIc2ca;
import ic2ca.common.item.ItemNanoBow;
import ic2ca.common.item.ItemUpgrade;
import ic2ca.common.item.armor.ItemArmorBaseJetpack;
import ic2ca.common.item.armor.ItemArmorExoBatpack;
import ic2ca.common.item.armor.ItemArmorExoEnergypack;
import ic2ca.common.item.armor.ItemArmorExoJetpack;
import ic2ca.common.item.armor.ItemArmorExoAdvBatpack;
import ic2ca.common.item.armor.ItemArmorExoNano;
import ic2ca.common.item.armor.ItemArmorExoQuantum;
import ic2ca.common.item.armor.ItemArmorExoSolar;
import ic2ca.common.item.armor.ItemArmorExoStatic;
import ic2ca.common.item.armor.ItemBodyJetpackBatpack;
import ic2ca.common.item.armor.ItemBodyJetpackAdvBatpack;
import ic2ca.common.item.armor.ItemBodyJetpackEnergypack;
import ic2ca.common.item.armor.ItemBodyNanoBatpack;
import ic2ca.common.item.armor.ItemBodyNanoEnergypack;
import ic2ca.common.item.armor.ItemBodyNanoJetpack;
import ic2ca.common.item.armor.ItemBodyNanoAdvBatpack;
import ic2ca.common.item.armor.ItemBodyNanoUltimate;
import ic2ca.common.item.armor.ItemBodyQuantumBatpack;
import ic2ca.common.item.armor.ItemBodyQuantumEnergypack;
import ic2ca.common.item.armor.ItemBodyQuantumJetpack;
import ic2ca.common.item.armor.ItemBodyQuantumAdvBatpack;
import ic2ca.common.item.armor.ItemBodyQuantumUltimate;
import ic2ca.common.item.armor.ItemBootsStaticNano;
import ic2ca.common.item.armor.ItemBootsStaticQuantum;
import ic2ca.common.item.armor.ItemHelmetNanoSolar;
import ic2ca.common.item.armor.ItemHelmetQuantumSolar;
import ic2ca.common.tileentity.TileEntityArmorAssembler;
import ic2ca.common.tileentity.TileEntityArmorCharger;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Random;
import java.util.logging.Logger;



import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.Property;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.ICraftingHandler;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkMod.SidedPacketHandler;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;

@Mod(modid = Util.MOD_ID, name = Util.MOD_NAME, dependencies = Util.DEPENDENCIES, version = Util.VERSION)
@NetworkMod(clientSideRequired = true, serverSideRequired = false, clientPacketHandlerSpec = @SidedPacketHandler(channels = { Util.CHANNEL }, packetHandler = ClientPacketHandleric2ca.class), serverPacketHandlerSpec = @SidedPacketHandler(channels = { Util.CHANNEL }, packetHandler = CommonPacketHandleric2ca.class))
public class IC2CA implements ITickHandler, ICraftingHandler
{
	@SidedProxy(clientSide="ic2ca.client.ClientProxyic2ca", serverSide="ic2ca.common.CommonProxyic2ca")
	public static CommonProxyic2ca proxy;
	public static Logger ic2caLog;
	public static ClientTickHandleric2ca clientTickHandler;
	public static ServerTickHandleric2ca serverTickHandler;
	public static final Side side = FMLCommonHandler.instance().getEffectiveSide();
	public static Random random = new Random();
	@Instance("IC2CA")
	public static IC2CA instance;
	public static CreativeTabIC2CA tabIC2CA = new CreativeTabIC2CA();
	public static ArrayList solars = new ArrayList();
	public static ArrayList statics = new ArrayList();
	public static ArrayList chests = new ArrayList();
	public static int assemblerID;
	public static int chargerID;
	public static int nanoBowID;
	public static int drillID;
	public static int drillBitID;
	public static int solarNanoHelmID;
	public static int solarQuantumHelmID;
	public static int nanoBatpackID;
	public static int nanoAdvBatpackID;
	public static int nanoEnergypackID;
	public static int nanoJetpackID;
	public static int nanoUltimateID;
	public static int quantumBatpackID;
	public static int quantumAdvBatpackID;
	public static int quantumEnergypackID;
	public static int quantumJetpackID;
	public static int quantumUltimateID;
	public static int jetpackBatpackID;
	public static int jetpackAdvBatpackID;
	public static int jetpackEnergypackID;  
	public static int nanoStaticID;
	public static int quantumStaticID;
	public static int exoNanoHelmID;
	public static int exoNanoChestID;
	public static int exoNanoLegsID;
	public static int exoNanoBootsID;
	public static int exoQuantumHelmID;
	public static int exoQuantumChestID;
	public static int exoQuantumLegsID;
	public static int exoQuantumBootsID;
	public static int exoJetpackID;
	public static int exoBatpackID;
	public static int exoAdvBatpackID;
	public static int exoEnergypackID;  
	public static int exoSolarID;
	public static int exoStaticID;
	public static int exoModuleID;
	public static int jetBoosterID;
	public static int flightModuleID;
	public static int solarModuleID;
	public static int staticModuleID;
	public static int cloakingModuleID;
	public static int overchargeModuleID;
	public static int energyMk2ID;
	public static int energyMk3ID;
	public static int lvSolarModuleID;
	public static int mvSolarModuleID;
	public static int hvSolarModuleID;
	public static int speedBoosterID;
	public static int opCardID;
	public static int soPriority1;
	public static int soPriority2;
	public static int soPriority3;
	public static int soPriority4;
	public static int stPriority1;
	public static int stPriority2;
	public static int stPriority3;
	public static int stPriority4;
	public static int maxProdUpgrades;
	public static int maxEnergyUpgrades;
	public static int maxTransferUpgrades;
	public static int nanoBowBoost;
	public static int turbineEUAmount;
	public static int jetpackEUAmount;
	public static boolean craftSolarProd;
	public static boolean craftStaticProd;
	public static boolean craftFlightTurbine;
	public static boolean craftCloakingModule;
	public static boolean craftDischargeModule;
	public static boolean craftEnergyMk2;
	public static boolean craftEnergyMk3;
	public static boolean useEnergyMk1;
	public static boolean useOverclocker;
	public static boolean useTransformer;
	public static boolean craftNanoBow;
	public static boolean normalMode;
	public static boolean rapidFireMode;
	public static boolean spreadMode;
	public static boolean sniperMode;
	public static boolean flameMode;
	public static boolean explosiveMode;

	public static boolean tryChargeSolar(EntityPlayer player, int slot, int prod)
	{
		return ElectricItem.manager.charge(player.inventory.armorInventory[slot], prod, 2147483647, true, false) > 0;
	}

	public static boolean tryChargeStatic(EntityPlayer var1, int slot, double var5, int prod)
	{
		return ElectricItem.manager.charge(var1.inventory.armorInventory[slot], Math.min(3, (int)var5 / 5) + prod, 2147483647, true, false) > 0;
	}

	public String getLabel()
	{
		return "IC2-CA";
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event)
	{
		proxy.registerThings();
		registerCraftingRecipes();
	}

	public void onCrafting(EntityPlayer player, ItemStack output, IInventory iinventory)
	{
		if (output != null && output.getItem() instanceof IItemUpgradeable)
		{
			output.setTagCompound(null);
			NBTTagCompound nbtout = StackUtil.getOrCreateNbtData(output);
			for (int i = 0; i < iinventory.getSizeInventory(); ++i) 
			{
				if (iinventory.getStackInSlot(i) != null)
				{
					ItemStack input = iinventory.getStackInSlot(i);
					NBTTagCompound nbtin = StackUtil.getOrCreateNbtData(input);
					if ((input.isItemEqual(Ic2caItems.flightModule)) && (output.getItem() instanceof ItemArmorBaseJetpack)) 
					{
						nbtout.setBoolean("flight", true);
					}
					if ((input.isItemEqual(Ic2caItems.cloakingModule)) && (chests.contains(output.getItem().itemID))) 
					{
						nbtout.setBoolean("cloaking", true);
					}
					if ((input.isItemEqual(Ic2caItems.overchargeModule)) && (chests.contains(output.getItem().itemID))) 
					{
						nbtout.setBoolean("overcharge", true);
					}
					if ((input.isItemEqual(Ic2caItems.solarModule)) && (solars.contains(output.getItem().itemID)))
					{
						int prod = nbtout.getInteger("solarProd");
						prod += input.stackSize;
						input.stackSize = 0;
						if (prod > maxProdUpgrades) 
						{
							prod = maxProdUpgrades;
						}
						nbtout.setInteger("solarProd", prod);
					}
					if ((Ic2caItems.lvSolarModule != null) && (input.isItemEqual(Ic2caItems.lvSolarModule)) && (solars.contains(output.getItem().itemID)))
					{
						int prod = nbtout.getInteger("solarProd");
						prod += input.stackSize * 8;
						input.stackSize = 0;
						if (prod > maxProdUpgrades) 
						{
							prod = maxProdUpgrades;
						}
						nbtout.setInteger("solarProd", prod);
					}
					if ((Ic2caItems.mvSolarModule != null) && (input.isItemEqual(Ic2caItems.mvSolarModule)) && (solars.contains(output.getItem().itemID)))
					{
						int prod = nbtout.getInteger("solarProd");
						prod += input.stackSize * 64;
						input.stackSize = 0;
						if (prod > maxProdUpgrades) 
						{
							prod = maxProdUpgrades;
						}
						nbtout.setInteger("solarProd", prod);
					}
					if ((Ic2caItems.hvSolarModule != null) && (input.isItemEqual(Ic2caItems.hvSolarModule)) && (solars.contains(output.getItem().itemID)))
					{
						int prod = nbtout.getInteger("solarProd");
						prod += input.stackSize * 512;
						input.stackSize = 0;
						if (prod > maxProdUpgrades) 
						{
							prod = maxProdUpgrades;
						}
						nbtout.setInteger("solarProd", prod);
					}
					if ((input.isItemEqual(Ic2caItems.staticModule)) && (statics.contains(output.getItem().itemID)))
					{
						int prod = nbtout.getInteger("staticProd");
						prod += input.stackSize;
						input.stackSize = 0;
						if (prod > maxProdUpgrades) 
						{
							prod = maxProdUpgrades;
						}
						nbtout.setInteger("staticProd", prod);
					}
					if ((input.isItemEqual(Ic2Items.energyStorageUpgrade)) && (output.getItem() instanceof IElectricItem) && (output.getItem() instanceof IItemUpgradeable))
					{
						IItemUpgradeable outputItem = (IItemUpgradeable)output.getItem();
						int var10 = nbtout.getInteger("upgradedCharge");
						var10 += input.stackSize * 10000;
						if (var10 > outputItem.getMaxUpgradeableCharge()) 
						{
							var10 = outputItem.getMaxUpgradeableCharge();
						}
						input.stackSize = 0;
						nbtout.setInteger("upgradedCharge", var10);
						nbtout.setInteger("maxCharge", outputItem.getDefaultMaxCharge() + var10);
						updateElectricDamageBars(output);
					}
					if ((input.isItemEqual(Ic2caItems.energyMk2)) && (output.getItem() instanceof IElectricItem) && (output.getItem() instanceof IItemUpgradeable))
					{
						IItemUpgradeable outputItem = (IItemUpgradeable)output.getItem();
						int var10 = nbtout.getInteger("upgradedCharge");
						var10 += input.stackSize * 100000;
						if (var10 > outputItem.getMaxUpgradeableCharge()) 
						{
							var10 = outputItem.getMaxUpgradeableCharge();
						}
						input.stackSize = 0;
						nbtout.setInteger("upgradedCharge", var10);
						nbtout.setInteger("maxCharge", outputItem.getDefaultMaxCharge() + var10);
						updateElectricDamageBars(output);
					}
					if ((input.isItemEqual(Ic2caItems.energyMk3)) && (output.getItem() instanceof IElectricItem) && (output.getItem() instanceof IItemUpgradeable))
					{
						IItemUpgradeable outputItem = (IItemUpgradeable)output.getItem();
						int var10 = nbtout.getInteger("upgradedCharge");
						var10 += input.stackSize * 1000000;
						if (var10 > outputItem.getMaxUpgradeableCharge()) 
						{
							var10 = outputItem.getMaxUpgradeableCharge();
						}
						input.stackSize = 0;
						nbtout.setInteger("upgradedCharge", var10);
						nbtout.setInteger("maxCharge", outputItem.getDefaultMaxCharge() + var10);
						updateElectricDamageBars(output);
					}
					if ((input.isItemEqual(Ic2Items.overclockerUpgrade)) && (output.getItem() instanceof IElectricItem) && (output.getItem() instanceof IItemUpgradeable))
					{
						IItemUpgradeable outputItem = (IItemUpgradeable)output.getItem();
						int var10 = nbtout.getInteger("upgradedTransfer");
						var10 += input.stackSize * 100;
						if (var10 > outputItem.getMaxUpgradeableTransfer())
						{
							var10 = outputItem.getMaxUpgradeableTransfer();
						}
						input.stackSize = 0;
						nbtout.setInteger("upgradedTransfer", var10);
						nbtout.setInteger("transferLimit", outputItem.getDefaultTransferLimit() + var10);
						updateElectricDamageBars(output);
					}
					if ((input.isItemEqual(Ic2Items.transformerUpgrade)) && (output.getItem() instanceof IElectricItem) && (output.getItem() instanceof IItemUpgradeable))
					{
						IItemUpgradeable outputItem = (IItemUpgradeable)output.getItem();
						int var10 = nbtout.getInteger("upgradedTier");
						var10 += input.stackSize;
						if (outputItem.getDefaultTier() - var10 < 1)
						{
							var10 = outputItem.getDefaultTier() - 1;
						}
						input.stackSize = 0;
						nbtout.setInteger("upgradedTier", var10);
						nbtout.setInteger("tier", outputItem.getDefaultTier() - var10);
						if (nbtout.getInteger("tier") < 1) 
						{
							nbtout.setInteger("tier", 1);
						}
						updateElectricDamageBars(output);
					}
					if ((input.getItem() instanceof IElectricItem) && (input.getItem() instanceof IItemUpgradeable) && (output.getItem() instanceof IElectricItem) && (output.getItem() instanceof IItemUpgradeable))
					{
						IItemUpgradeable outputItem = (IItemUpgradeable)output.getItem();

						int var10 = nbtout.getInteger("upgradedCharge");
						var10 += nbtin.getInteger("upgradedCharge");
						if (var10 > outputItem.getMaxUpgradeableCharge()) 
						{
							var10 = outputItem.getMaxUpgradeableCharge();
						}
						input.stackSize = 0;
						nbtout.setInteger("upgradedCharge", var10);
						nbtout.setInteger("maxCharge", outputItem.getDefaultMaxCharge() + var10);

						int var11 = nbtout.getInteger("upgradedTransfer");
						var11 += nbtin.getInteger("upgradedTransfer");
						if (var11 > outputItem.getMaxUpgradeableTransfer()) 
						{
							var11 = outputItem.getMaxUpgradeableTransfer();
						}
						input.stackSize = 0;
						nbtout.setInteger("upgradedTransfer", var11);
						nbtout.setInteger("transferLimit", outputItem.getDefaultTransferLimit() + var11);

						int out = nbtout.getInteger("upgradedTier");
						out += nbtin.getInteger("upgradedTier");
						if (outputItem.getDefaultTier() - out < 1) 
						{
							out = outputItem.getDefaultTier() - 1;
						}
						input.stackSize = 0;
						nbtout.setInteger("upgradedTier", out);
						nbtout.setInteger("tier", outputItem.getDefaultTier() - out);
						if (nbtout.getInteger("tier") < 1) 
						{
							nbtout.setInteger("tier", 1);
						}
						updateElectricDamageBars(output);
					}
					if ((solars.contains(input.getItem().itemID)) && (solars.contains(output.getItem().itemID)))
					{
						int var8 = nbtout.getInteger("solarProd");
						var8 += nbtin.getInteger("solarProd");
						if (ModIntegrationHandler.isModLoaded(0)) 
						{
							if (input.itemID == Ic2caItems.lvHat.itemID) 
							{
								var8 += 7;
							} else if (input.itemID == Ic2caItems.mvHat.itemID) 
							{
								var8 += 63;
							} else if (input.itemID == Ic2caItems.hvHat.itemID) 
							{
								var8 += 511;
							}
						}
						input.stackSize = 0;
						if (var8 > maxProdUpgrades) 
						{
							var8 = maxProdUpgrades;
						}
						nbtout.setInteger("solarProd", var8);
					}
					if ((statics.contains(input.getItem().itemID)) && (statics.contains(output.getItem().itemID)))
					{
						int var8 = nbtout.getInteger("staticProd");
						var8 += nbtin.getInteger("staticProd");
						input.stackSize = 0;
						if (var8 > maxProdUpgrades) 
						{
							var8 = maxProdUpgrades;
						}
						nbtout.setInteger("staticProd", var8);
					}
					if ((input.getItem() instanceof ItemArmorBaseJetpack) && (output.getItem() instanceof ItemArmorBaseJetpack) && (nbtin.getBoolean("flight"))) 
					{
						nbtout.setBoolean("flight", true);
					}
					if (chests.contains(input.getItem().itemID) && chests.contains(output.getItem().itemID) && (nbtin.getBoolean("cloaking"))) 
					{
						nbtout.setBoolean("cloaking", true);
					}
					if (chests.contains(input.getItem().itemID) && chests.contains(output.getItem().itemID) && (nbtin.getBoolean("overcharge"))) 
					{
						nbtout.setBoolean("overcharge", true);
					}
				}
			}
		}
	}

	public void onSmelting(EntityPlayer player, ItemStack item) {}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
		ModIntegrationHandler.loadIntegrationModules();
	}

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		ic2caLog = event.getModLog();

		// Load configuration file
		final String CATEGORY_CROSSMOD = "cross-mod";
		final String CATEGORY_GENERAL = Configuration.CATEGORY_GENERAL;

		Configuration config = new Configuration(event.getSuggestedConfigurationFile());
		try
		{
			config.load();

			exoNanoHelmID = config.getItem("exoNanoHelm", 30500).getInt();
			exoNanoChestID = config.getItem("exoNanoChest", 30501).getInt();
			exoNanoLegsID = config.getItem("exoNanoLegs", 30502).getInt();
			exoNanoBootsID = config.getItem("exoNanoBoots", 30503).getInt();
			exoQuantumHelmID = config.getItem("exoQuantumHelm", 30504).getInt();
			exoQuantumChestID = config.getItem("exoQuantumChest", 30505).getInt();
			exoQuantumLegsID = config.getItem("exoQuantumLegs", 30506).getInt();
			exoQuantumBootsID = config.getItem("exoQuantumBoots", 30507).getInt();
			exoJetpackID = config.getItem("exoJetpack", 30508).getInt();
			exoBatpackID = config.getItem("exoBatpack", 30509).getInt();
			exoAdvBatpackID = config.getItem("exoAdvBatpack", 30510).getInt();    
			exoSolarID = config.getItem("exoSolar", 30511).getInt();
			exoStaticID = config.getItem("exoStatic", 30512).getInt();
			solarNanoHelmID = config.getItem("solarNano", 30513).getInt();
			solarQuantumHelmID = config.getItem("solarQuantum", 30514).getInt();
			nanoBatpackID = config.getItem("nanoBatpack", 30515).getInt();
			nanoAdvBatpackID = config.getItem("nanoAdvBatpack", 30516).getInt();
			nanoJetpackID = config.getItem("nanoJetpack", 30517).getInt();
			nanoUltimateID = config.getItem("nanoUltimate", 30518).getInt();
			quantumBatpackID = config.getItem("quantumBatpack", 30519).getInt();
			quantumAdvBatpackID = config.getItem("quantumAdvBatpack", 30520).getInt();
			quantumJetpackID = config.getItem("quantumJetpack", 30521).getInt();
			quantumUltimateID = config.getItem("quantumUltimate", 30522).getInt();
			jetpackBatpackID = config.getItem("jetpackBatpack", 30523).getInt();
			jetpackAdvBatpackID = config.getItem("jetpackAdvBatpack", 30524).getInt();
			nanoStaticID = config.getItem("nanoStatic", 30525).getInt();
			quantumStaticID = config.getItem("quantumStatic", 30526).getInt();
			exoModuleID = config.getItem("exoModule", 30527).getInt();
			jetBoosterID = config.getItem("jetBooster", 30528).getInt();
			flightModuleID = config.getItem("flightModule", 30529).getInt();
			solarModuleID = config.getItem("solarModule", 30530).getInt();
			staticModuleID = config.getItem("staticModule", 30531).getInt();
			cloakingModuleID = config.getItem("cloakingModule", 30532).getInt();
			overchargeModuleID = config.getItem("overchargeModule", 30533).getInt();
			nanoBowID = config.getItem("nanoBow", 30534).getInt();
			energyMk2ID = config.getItem("energyMk2", 30535).getInt();
			energyMk3ID = config.getItem("energyMk3", 30536).getInt();
			assemblerID = config.getBlock("armorAssembler", 4000).getInt();
			chargerID = config.getBlock("armorCharger", 4001).getInt();
			drillID = config.getItem("drill", 30537).getInt();
			drillBitID = config.getItem("drillBit", 30538).getInt();
			speedBoosterID = config.getItem("assemblerSpeedBooster", 30542).getInt();
			opCardID = config.getItem("assemblerOPUpgrade", 30543).getInt();
			exoEnergypackID = config.getItem("exoEnergypack", 30544).getInt();
			nanoEnergypackID = config.getItem("nanoEnergypack", 30545).getInt();
			quantumEnergypackID = config.getItem("quantumEnergypack", 30546).getInt();
			jetpackEnergypackID = config.getItem("jetpackEnergypack", 30547).getInt();

			Property enableGSuite = config.get(CATEGORY_CROSSMOD, "gs-enable", true);
			enableGSuite.comment = "Disable GraviSuite integration, regardless of whether or not the mod is found.";
			ModIntegrationHandler.setIntegrationEnabled(ModIntegrationHandler.GRAVISUITE, enableGSuite.getBoolean(true));

			Property enableCSolars = config.get(CATEGORY_CROSSMOD, "cs-enable", true);
			enableCSolars.comment = "Disable Compact Solars integration, regardless of whether or not the mod is found.";
			ModIntegrationHandler.setIntegrationEnabled(ModIntegrationHandler.COMPACT_SOLARS, enableCSolars.getBoolean(true));

			Property enableASolars = config.get(CATEGORY_CROSSMOD, "asp-enable", true);
			enableASolars.comment = "Disabled Advanced Solar Panels integration, regardless of whether or not the mod is found.";
			ModIntegrationHandler.setIntegrationEnabled(ModIntegrationHandler.ADVANCED_SOLAR_PANELS, enableASolars.getBoolean(true));

			Property hvSolarProp = config.get(CATEGORY_CROSSMOD, "cs/asp-hvSolarModuleID", 30541);
			hvSolarProp.comment = "IDs for the LV, MV, and HV Solar Modules for CompactSolars and ASP Integration. If compact solars and ASP are both not installed, or integration is disabled, these IDs are unused.";
			hvSolarModuleID = hvSolarProp.getInt();
			lvSolarModuleID = config.get(CATEGORY_CROSSMOD, "cs/asp-lvSolarModuleID", 30539).getInt();
			mvSolarModuleID = config.get(CATEGORY_CROSSMOD, "cs/asp-mcSolarModuleID", 30540).getInt();

			Property soPriority1Prop = config.get(CATEGORY_GENERAL, "solarPriority1", 2);
			soPriority1Prop.comment = "Set the charging priority for the Solar Helmets and Static Boots. Use numbers 0-3, where 0 is the boots. Default order: 2, 0, 1, 3";
			soPriority1 = soPriority1Prop.getInt();
			soPriority2 = config.get(CATEGORY_GENERAL, "solarPriority2", 0).getInt();
			soPriority3 = config.get(CATEGORY_GENERAL, "solarPriority3", 1).getInt();
			soPriority4 = config.get(CATEGORY_GENERAL, "solarPriority4", 3).getInt();
			stPriority1 = config.get(CATEGORY_GENERAL, "staticPriority1", 2).getInt();
			stPriority2 = config.get(CATEGORY_GENERAL, "staticPriority2", 0).getInt();
			stPriority3 = config.get(CATEGORY_GENERAL, "staticPriority3", 1).getInt();
			stPriority4 = config.get(CATEGORY_GENERAL, "staticPriority4", 3).getInt();

			turbineEUAmount = config.get(CATEGORY_GENERAL, "euUsageTurbine", 10).getInt();
			Property jetpackTurbine = config.get(CATEGORY_GENERAL, "euUsageJetpack", 8);
			jetpackTurbine.comment = "Change the EU Usage of Jetpacks and Turbines. Hover mode uses 25% less than the value below.";
			jetpackEUAmount = jetpackTurbine.getInt(8);

			Property maxProdUpgradesProp = config.get(CATEGORY_GENERAL, "maxProductionUpgrades", 511);
			maxProdUpgradesProp.comment = "Set the max number of Solar Production or Static Production upgrades to be installed in one item. Note that the max will be one more than the number you enter, as the default has 1. Default value: 511.";
			maxProdUpgrades = maxProdUpgradesProp.getInt();

			Property maxEnergyUpgradesProp = config.get(CATEGORY_GENERAL, "maxEnergyUpgrades", 100000000);
			maxEnergyUpgradesProp.comment = "Set the max Energy that an upgraded item can have. Default: 100,000,000";
			maxEnergyUpgrades = maxEnergyUpgradesProp.getInt();
			Property maxTransferUpgradesProp = config.get(CATEGORY_GENERAL, "maxTransferUpgrades", 200000);
			maxTransferUpgradesProp.comment = "Set the max Transfer Limit that an upgraded item can have. Default: 200,000";
			maxTransferUpgrades = maxTransferUpgradesProp.getInt();

			Property enableCraftingCloak = config.get(CATEGORY_GENERAL, "enableCraftingCloakingModule", true);
			enableCraftingCloak.comment = "Enable whether or not the upgrades can be crafted. They can still be spawned in and used if you are an admin. Default: all true.";
			craftSolarProd = config.get(CATEGORY_GENERAL, "enableCraftingSolarProduction", true).getBoolean(true);
			craftStaticProd = config.get(CATEGORY_GENERAL, "enableCraftingStaticProduction", true).getBoolean(true);
			craftFlightTurbine = config.get(CATEGORY_GENERAL, "enableCraftingFlightTurbine", true).getBoolean(true);
			craftCloakingModule = enableCraftingCloak.getBoolean(true);
			craftDischargeModule = config.get(CATEGORY_GENERAL, "enableCraftingDischargeModule", true).getBoolean(true);
			craftEnergyMk2 = config.get(CATEGORY_GENERAL, "enableCraftingEnergyMk2", true).getBoolean(true);
			craftEnergyMk3 = config.get(CATEGORY_GENERAL, "enableCraftingEnergyMk3", true).getBoolean(true);
			craftNanoBow = config.get(CATEGORY_GENERAL, "enableCraftingNanoBow", true).getBoolean(true);
			useEnergyMk1 = config.get(CATEGORY_GENERAL, "enableUseEnergyMk1", true).getBoolean(true);
			useOverclocker = config.get(CATEGORY_GENERAL, "enableUseOverclocker", true).getBoolean(true);
			useTransformer = config.get(CATEGORY_GENERAL, "enableUseTransformer", true).getBoolean(true);

			Property nanoboost = config.get(CATEGORY_GENERAL, "nanoBowDamageBoost", 0);
			nanoboost.comment = "Boost the damage of the NanoBow, for use with things like Divine RPG. Each number adds 1 level of the Power enchantment.";
			nanoBowBoost = nanoboost.getInt();
			
			Property nanoBowMods = config.get(CATEGORY_GENERAL, "bowExplosiveMode", true);
			nanoBowMods.comment = "Enabled NanoBow modes { \"Normal\", \"Rapid fire\", \"Spread\", \"Sniper\", \"Flame\", \"Explosive\" }";
			explosiveMode = nanoBowMods.getBoolean(true);
			flameMode = config.get(CATEGORY_GENERAL, "bowFlameMode", true).getBoolean(true);
			//normalMode = config.get(CATEGORY_GENERAL, "bowNormalMode", true).getBoolean(true);
			rapidFireMode = config.get(CATEGORY_GENERAL, "bowRapidFireMode", true).getBoolean(true);
			sniperMode = config.get(CATEGORY_GENERAL, "bowSniperMode", true).getBoolean(true);
			spreadMode = config.get(CATEGORY_GENERAL, "bowSpreadMode", true).getBoolean(true);
		}
		catch (Exception e)
		{
			ic2caLog.info("IC2CA has a problem loading it's configuration");
		}
		finally
		{
			config.save();
		}
		
		TickRegistry.registerTickHandler(this, Side.SERVER);
		TickRegistry.registerTickHandler(new ServerTickHandleric2ca(), Side.SERVER);
		EntityRegistry.registerGlobalEntityID(EntityTechArrow.class, "TechArrow", EntityRegistry.findGlobalUniqueEntityId());
		EntityRegistry.registerGlobalEntityID(EntityLaser.class, "HelmetLaser", EntityRegistry.findGlobalUniqueEntityId());
		registerItems();

		Ic2caItems.armorAssembler = new ItemStack(new BlockArmorAssembler(assemblerID, false));
		Ic2caItems.armorCharger = new ItemStack(new BlockArmorCharger(chargerID, false));
		GameRegistry.registerTileEntity(TileEntityArmorAssembler.class, "Armor Assembler");
		GameRegistry.registerTileEntity(TileEntityArmorCharger.class, "Armor Charger");
		NetworkRegistry.instance().registerGuiHandler(this, new GuiHandlerIc2ca());

		solars.add(Ic2caItems.solarNanoHelm.itemID);
		solars.add(Ic2caItems.solarQuantumHelm.itemID);
		solars.add(Ic2caItems.exoSolar.itemID);

		statics.add(Ic2caItems.nanoStatic.itemID);
		statics.add(Ic2caItems.quantumStatic.itemID);
		statics.add(Ic2caItems.exoStatic.itemID);

		chests.add(Ic2caItems.exoNanoChest.itemID);
		chests.add(Ic2caItems.exoQuantumChest.itemID);
		chests.add(Ic2caItems.exoJetpack.itemID);
		chests.add(Ic2caItems.exoAdvBatpack.itemID);
		chests.add(Ic2caItems.exoEnergypack.itemID);
		chests.add(Ic2caItems.exoBatpack.itemID);
		chests.add(Ic2caItems.nanoBatpack.itemID);
		chests.add(Ic2caItems.nanoAdvBatpack.itemID);
		chests.add(Ic2caItems.nanoEnergypack.itemID);
		chests.add(Ic2caItems.nanoJetpack.itemID);
		chests.add(Ic2caItems.nanoUltimate.itemID);
		chests.add(Ic2caItems.quantumBatpack.itemID);
		chests.add(Ic2caItems.quantumAdvBatpack.itemID);
		chests.add(Ic2caItems.quantumEnergypack.itemID);
		chests.add(Ic2caItems.quantumJetpack.itemID);
		chests.add(Ic2caItems.quantumUltimate.itemID);
		chests.add(Ic2caItems.jetpackBatpack.itemID);
		chests.add(Ic2caItems.jetpackAdvBatpack.itemID);
		chests.add(Ic2caItems.jetpackEnergypack.itemID);
	}

	public void registerCraftingRecipes()
	{
		RecipeHandler.instance().addComboRecipes();
		RecipeHandler.instance().addCraftingRecipes();
		if (craftFlightTurbine)
		{
			Recipes.advRecipes.addRecipe(Ic2caItems.jetBooster, new Object[] { "RAR", "RIR", "G G", Character.valueOf('R'), "plateIron", Character.valueOf('A'), Ic2Items.advancedCircuit, Character.valueOf('I'), Ic2Items.iridiumPlate, Character.valueOf('G'), Item.glowstone });
			Recipes.advRecipes.addRecipe(Ic2caItems.flightModule, new Object[] { "RAR", "BLB", "RAR", Character.valueOf('R'), "plateIron", Character.valueOf('A'), Ic2Items.advancedCircuit, Character.valueOf('L'), StackUtil.copyWithWildCard(Ic2Items.lapotronCrystal), Character.valueOf('B'), Ic2caItems.jetBooster });
		}
		RecipeHandler.instance().addJetpackRecipes(Ic2caItems.flightModule);
		if (craftSolarProd)
		{
			Recipes.advRecipes.addRecipe(Ic2caItems.solarModule, new Object[] { "RRR", "CSC", "RRR", Character.valueOf('R'), "plateIron", Character.valueOf('C'), Ic2Items.insulatedCopperCableItem, Character.valueOf('S'), Ic2Items.solarPanel });
		}
		RecipeHandler.instance().addSolarRecipes(Ic2caItems.solarModule);
		if (craftStaticProd)
		{
			Recipes.advRecipes.addRecipe(Ic2caItems.staticModule, new Object[] { "RWR", "CEC", "RWR", Character.valueOf('R'), "plateIron", Character.valueOf('W'), Block.cloth, Character.valueOf('C'), Ic2Items.insulatedCopperCableItem, Character.valueOf('E'), Ic2Items.electronicCircuit });
		}
		RecipeHandler.instance().addStaticRecipes(Ic2caItems.staticModule);
		if (craftCloakingModule)
		{
			Recipes.advRecipes.addRecipe(Ic2caItems.cloakingModule, new Object[] { "RAR", "CIC", "RAR", Character.valueOf('R'), "plateIron", Character.valueOf('C'), Item.goldenCarrot, Character.valueOf('A'), Ic2Items.advancedCircuit, Character.valueOf('I'), Ic2Items.iridiumPlate });
		}
		RecipeHandler.instance().addChestpieceRecipes(Ic2caItems.cloakingModule);
		if (craftDischargeModule)
		{
			Recipes.advRecipes.addRecipe(Ic2caItems.overchargeModule, new Object[] { "RAR", "TIT", "RAR", Character.valueOf('R'), "plateIron", Character.valueOf('T'), Ic2Items.teslaCoil, Character.valueOf('A'), Ic2Items.advancedCircuit, Character.valueOf('I'), Ic2Items.iridiumPlate });
		}
		RecipeHandler.instance().addChestpieceRecipes(Ic2caItems.overchargeModule);
		if (useOverclocker)
		{
			RecipeHandler.instance().addElectricRecipes(Ic2Items.overclockerUpgrade);
		}
		if (useEnergyMk1)
		{
			RecipeHandler.instance().addElectricRecipes(Ic2Items.energyStorageUpgrade);
		}
		if (useTransformer)
		{
			RecipeHandler.instance().addElectricRecipes(Ic2Items.transformerUpgrade);
		}
		if (craftEnergyMk2)
		{
			Recipes.advRecipes.addRecipe(Ic2caItems.energyMk2, new Object[] { "WWW", "GEG", "WCW", Character.valueOf('W'), Block.planks, Character.valueOf('G'), Ic2Items.insulatedGoldCableItem, Character.valueOf('E'), StackUtil.copyWithWildCard(Ic2Items.energyCrystal), Character.valueOf('C'), Ic2Items.electronicCircuit });
		}
		RecipeHandler.instance().addElectricRecipes(Ic2caItems.energyMk2);
		if (craftEnergyMk3)
		{
			Recipes.advRecipes.addRecipe(Ic2caItems.energyMk3, new Object[] { "WWW", "GEG", "WCW", Character.valueOf('W'), Block.planks, Character.valueOf('G'), Ic2Items.glassFiberCableItem, Character.valueOf('E'), StackUtil.copyWithWildCard(Ic2Items.lapotronCrystal), Character.valueOf('C'), Ic2Items.advancedCircuit });
		}
		RecipeHandler.instance().addElectricRecipes(Ic2caItems.energyMk3);
	}

	public void registerItems()
	{
		int renderExoNano = proxy.addArmor("ic2ca/exonano");
		int renderExoQuantum = proxy.addArmor("ic2ca/exoquantum");

		Ic2caItems.exoNanoHelm = new ItemStack(new ItemArmorExoNano(exoNanoHelmID, "exonanohelm", renderExoNano, 0));
		Ic2caItems.exoNanoChest = new ItemStack(new ItemArmorExoNano(exoNanoChestID, "exonanochest", renderExoNano, 1));
		Ic2caItems.exoNanoLegs = new ItemStack(new ItemArmorExoNano(exoNanoLegsID, "exonanolegs", renderExoNano, 2));
		Ic2caItems.exoNanoBoots = new ItemStack(new ItemArmorExoNano(exoNanoBootsID, "exonanoboots", renderExoNano, 3));
		Ic2caItems.exoQuantumHelm = new ItemStack(new ItemArmorExoQuantum(exoQuantumHelmID, "exoQuantumhelm", renderExoQuantum, 0));
		Ic2caItems.exoQuantumChest = new ItemStack(new ItemArmorExoQuantum(exoQuantumChestID, "exoQuantumchest", renderExoQuantum, 1));
		Ic2caItems.exoQuantumLegs = new ItemStack(new ItemArmorExoQuantum(exoQuantumLegsID, "exoQuantumlegs", renderExoQuantum, 2));
		Ic2caItems.exoQuantumBoots = new ItemStack(new ItemArmorExoQuantum(exoQuantumBootsID, "exoQuantumboots", renderExoQuantum, 3));
		Ic2caItems.exoJetpack = new ItemStack(new ItemArmorExoJetpack(exoJetpackID, "exojetpack", proxy.addArmor("ic2ca/exojetpack")));
		Ic2caItems.exoBatpack = new ItemStack(new ItemArmorExoBatpack(exoBatpackID, "exobatpack", proxy.addArmor("ic2ca/exobatpack")));
		Ic2caItems.exoAdvBatpack = new ItemStack(new ItemArmorExoAdvBatpack(exoAdvBatpackID, "exoadvbatpack", proxy.addArmor("ic2ca/exoadvpack")));
		Ic2caItems.exoEnergypack = new ItemStack(new ItemArmorExoEnergypack(exoEnergypackID, "exoenergypack", proxy.addArmor("ic2ca/exoenergypack")));
		Ic2caItems.exoSolar = new ItemStack(new ItemArmorExoSolar(exoSolarID, "exosolar", proxy.addArmor("ic2ca/exosolar")));
		Ic2caItems.exoStatic = new ItemStack(new ItemArmorExoStatic(exoStaticID, "exostatic", proxy.addArmor("ic2ca/exostatic")));
		Ic2caItems.solarNanoHelm = new ItemStack(new ItemHelmetNanoSolar(solarNanoHelmID, "solarnanohelm", proxy.addArmor("ic2ca/solarnano")));
		Ic2caItems.solarQuantumHelm = new ItemStack(new ItemHelmetQuantumSolar(solarQuantumHelmID, "solarquantumhelm", proxy.addArmor("ic2ca/solarquantum")));
		Ic2caItems.nanoBatpack = new ItemStack(new ItemBodyNanoBatpack(nanoBatpackID, "nanobatpack", proxy.addArmor("ic2ca/nanobat")));
		Ic2caItems.nanoAdvBatpack = new ItemStack(new ItemBodyNanoAdvBatpack(nanoAdvBatpackID, "nanoadvbatpack", proxy.addArmor("ic2ca/nanoadv")));
		Ic2caItems.nanoEnergypack = new ItemStack(new ItemBodyNanoEnergypack(nanoEnergypackID, "nanoenergypack", proxy.addArmor("ic2ca/nanoenergy")));
		Ic2caItems.nanoJetpack = new ItemStack(new ItemBodyNanoJetpack(nanoJetpackID, "nanojetpack", proxy.addArmor("ic2ca/nanojet")));
		Ic2caItems.nanoUltimate = new ItemStack(new ItemBodyNanoUltimate(nanoUltimateID, "ultimatenano", proxy.addArmor("ic2ca/ultimatenano")));
		Ic2caItems.quantumBatpack = new ItemStack(new ItemBodyQuantumBatpack(quantumBatpackID, "quantumbatpack", proxy.addArmor("ic2ca/quantumbat")));
		Ic2caItems.quantumAdvBatpack = new ItemStack(new ItemBodyQuantumAdvBatpack(quantumAdvBatpackID, "quantumadvpack", proxy.addArmor("ic2ca/quantumadv")));
		Ic2caItems.quantumEnergypack = new ItemStack(new ItemBodyQuantumEnergypack(quantumEnergypackID, "quantumenergypack", proxy.addArmor("ic2ca/quantumenergy")));
		Ic2caItems.quantumJetpack = new ItemStack(new ItemBodyQuantumJetpack(quantumJetpackID, "quantumjetpack", proxy.addArmor("ic2ca/quantumjet")));
		Ic2caItems.quantumUltimate = new ItemStack(new ItemBodyQuantumUltimate(quantumUltimateID, "ultimatequantum", proxy.addArmor("ic2ca/ultimatequantum")));
		Ic2caItems.jetpackBatpack = new ItemStack(new ItemBodyJetpackBatpack(jetpackBatpackID, "jetpackbatpack", proxy.addArmor("ic2ca/batjetpack")));
		Ic2caItems.jetpackAdvBatpack = new ItemStack(new ItemBodyJetpackAdvBatpack(jetpackAdvBatpackID, "jetpackadvpack", proxy.addArmor("ic2ca/advjetpack")));
		Ic2caItems.jetpackEnergypack = new ItemStack(new ItemBodyJetpackEnergypack(jetpackEnergypackID, "jetpackenergypack", proxy.addArmor("ic2ca/energyjetpack")));
		Ic2caItems.nanoStatic = new ItemStack(new ItemBootsStaticNano(nanoStaticID, "nanostatic", proxy.addArmor("ic2ca/nanostatic")));
		Ic2caItems.quantumStatic = new ItemStack(new ItemBootsStaticQuantum(quantumStaticID, "quantumstatic", proxy.addArmor("ic2ca/quantumstatic")));

		Ic2caItems.exoModule = new ItemStack(new ItemIc2ca(exoModuleID, "exomodule", 64, 1));
		Ic2caItems.jetBooster = new ItemStack(new ItemIc2ca(jetBoosterID, "jetbooster", 64, 3));
		Ic2caItems.flightModule = new ItemStack(new ItemUpgrade(flightModuleID, "flightmodule", 1, 3, EnumUpgradeType.JETPACKS));
		Ic2caItems.solarModule = new ItemStack(new ItemUpgrade(solarModuleID, "solarmodule", 64, 2, EnumUpgradeType.SOLARS));
		Ic2caItems.staticModule = new ItemStack(new ItemUpgrade(staticModuleID, "staticmodule", 64, 2, EnumUpgradeType.STATICS));
		Ic2caItems.cloakingModule = new ItemStack(new ItemUpgrade(cloakingModuleID, "cloakingModule", 1, 3, EnumUpgradeType.CHESTS));
		Ic2caItems.overchargeModule = new ItemStack(new ItemUpgrade(overchargeModuleID, "overchargemodule", 1, 3, EnumUpgradeType.CHESTS));

		Ic2caItems.energyMk2 = new ItemStack(new ItemUpgrade(energyMk2ID, "energyMk2", 64, 1, EnumUpgradeType.ELECTRICS));
		Ic2caItems.energyMk3 = new ItemStack(new ItemUpgrade(energyMk3ID, "energyMk3", 64, 2, EnumUpgradeType.ELECTRICS));
		Ic2caItems.drill = new ItemStack(new ItemDrill(drillID,"assemblydrill"));
		Ic2caItems.drillBit = new ItemStack(new ItemIc2ca(drillBitID, "assemblydrillbit", 64, 1));

		Ic2caItems.nanoBow = new ItemStack(new ItemNanoBow(nanoBowID,"nanoBow"));

		Ic2caItems.speedBooster = new ItemStack(new ItemAssemblerUpgrade(speedBoosterID, "speedUpgrade", 3));
		Ic2caItems.opUpgrade = new ItemStack(new ItemAssemblerUpgrade(opCardID, "opAssemblerUpgrade", 4));
	}

	@Override
	public void tickEnd(EnumSet<TickType> type, Object... tickData) {}

	@Override
	public EnumSet<TickType> ticks()
	{
		return EnumSet.of(TickType.PLAYER);
	}

	@Override
	public void tickStart(EnumSet<TickType> type, Object... tickData)
	{
		if (type.contains(TickType.PLAYER))
		{
			EntityPlayer player = (EntityPlayer)tickData[0];
			if (player.isDead) 
			{
				return;
			}
		}
	}

	public void updateElectricDamageBars(ItemStack itemstack)
	{
		if (itemstack.getItem() instanceof IElectricItem)
		{
			NBTTagCompound nbt = StackUtil.getOrCreateNbtData(itemstack);
			if (nbt.getInteger("maxCharge") <= 0 && itemstack.getItem() instanceof IItemUpgradeable)
			{
				IItemUpgradeable item = (IItemUpgradeable)itemstack.getItem();
				nbt.setInteger("maxCharge", item.getDefaultMaxCharge());
			}
			int maxcharge = nbt.getInteger("maxCharge");
			int charge = nbt.getInteger("charge");
			if (itemstack.getItem() instanceof IElectricItem)
			{
				if (itemstack.getMaxDamage() > 2)
				{
					long a = (long)(maxcharge - charge);
					long b = (long)(a * (itemstack.getMaxDamage() - 2));
					long c = (long)(b / maxcharge);
					int d = (int)c;
					int p = d + 1;
					itemstack.setItemDamage(p);
				}
				else
				{
					itemstack.setItemDamage(0);
				}
			}
			else
			{
				itemstack.setItemDamage(0);
			}
		}
	}

	public void onPlayerLogout(EntityPlayer player)
	{
		if (IC2.platform.isSimulating())
		{
			ItemArmorExoQuantum.removePlayerReferences(player);
		}
	}
}
