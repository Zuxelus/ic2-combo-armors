package ic2ca.common.block;

import java.util.Random;

import cpw.mods.fml.common.registry.GameRegistry;
import ic2.core.Ic2Items;
import ic2ca.common.IC2CA;
import ic2ca.common.Util;
import ic2ca.common.tileentity.TileEntityArmorCharger;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class BlockArmorCharger extends BlockContainer
{
	  private Icon top;
	  private Icon bottom;
	  private Icon side;
	  private Icon front;
	  
	  public BlockArmorCharger(int i, boolean active)
	  {
	    super(i, Material.iron);
	    setHardness(3.0F);
	    setResistance(10.0F);
	    setStepSound(Block.soundMetalFootstep);
	    setCreativeTab(IC2CA.tabIC2CA);
	    setUnlocalizedName("armorCharger");
	    MinecraftForge.setBlockHarvestLevel(this, "wrench", 1);
	    //this.isActive = active;
	    GameRegistry.registerBlock(this, "ArmorChargerBlock");
	  }
	  public TileEntity createNewTileEntity(World world)
	  {
	    return new TileEntityArmorCharger();
	  }	  
	  
	  public int damageDropped(int i)
	  {
	    return Ic2Items.advancedMachine.getItemDamage();
	  }

	  private void dropItems(World world, int x, int y, int z)
	  {
	    Random rand = new Random();
	    
	    TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
	    if (!(tileEntity instanceof IInventory)) {
	      return;
	    }
	    IInventory inventory = (IInventory)tileEntity;
	    for (int i = 0; i < inventory.getSizeInventory(); i++)
	    {
	      ItemStack item = inventory.getStackInSlot(i);
	      if ((item != null) && (item.stackSize > 0))
	      {
	        float rx = rand.nextFloat() * 0.8F + 0.1F;
	        float ry = rand.nextFloat() * 0.8F + 0.1F;
	        float rz = rand.nextFloat() * 0.8F + 0.1F;
	        EntityItem entityItem = new EntityItem(world, x + rx, y + ry, z + rz, new ItemStack(item.itemID, item.stackSize, item.getItemDamage()));
	        if (item.hasTagCompound()) {
	          entityItem.getEntityItem().setTagCompound((NBTTagCompound)item.getTagCompound().copy());
	        }
	        float factor = 0.05F;
	        entityItem.motionX = (rand.nextGaussian() * factor);
	        entityItem.motionY = (rand.nextGaussian() * factor + 0.2000000029802322D);
	        entityItem.motionZ = (rand.nextGaussian() * factor);
	        world.spawnEntityInWorld(entityItem);
	        item.stackSize = 0;
	      }
	    }
	  }
	  
	  public Icon getBlockTexture(IBlockAccess iblockaccess, int x, int y, int z, int side)
	  {
	    if (side == 1) {
	      return this.top;
	    }
	    if (side == 0) {
	      return this.bottom;
	    }
	    int var6 = iblockaccess.getBlockMetadata(x, y, z);
	    if (side != var6) {
	      return this.side;
	    }
	    return this.front;
	  }
	  
	  public Icon getIcon(int i, int meta)
	  {
	    if (i == 0) {
	      return this.bottom;
	    }
	    if (i == 1) {
	      return this.top;
	    }
	    if (i == 3) {
	      return this.front;
	    }
	    return this.side;
	  }
	  
	  public int idDropped(int meta, Random random, int j)
	  {
	    return Ic2Items.advancedMachine.itemID;
	  }
	  
	  public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int idk, float what, float these, float are)
	  {
		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);	  
	    if ((tileEntity == null) || (player.isSneaking())) {
	      return false;
	    }
	    ItemStack item = player.inventory.getCurrentItem();    
	    if (item != null) {
	      if ((item.getItem().itemID == Ic2Items.wrench.itemID) || (item.getItem().itemID == Ic2Items.electricWrench.itemID)) {
	        return false;
	      }
	    }
	    player.openGui(IC2CA.instance, 0, world, x, y, z);
	    return true;   
	  }
	  
	  public void onBlockAdded(World par1World, int x, int y, int z)
	  {
	    super.onBlockAdded(par1World, x, y, z);
	    setDefaultDirection(par1World, x, y, z);
	  }
	  
	  public void onBlockPlacedBy(World par1World, int x, int y, int z, EntityLiving entityliving, ItemStack is)
	  {
	    int var6 = MathHelper.floor_double(entityliving.rotationYaw * 4.0F / 360.0F + 0.5D) & 0x3;
	    if (var6 == 0) {
	      par1World.setBlockMetadataWithNotify(x, y, z, 2, 2);
	    }
	    if (var6 == 1) {
	      par1World.setBlockMetadataWithNotify(x, y, z, 5, 2);
	    }
	    if (var6 == 2) {
	      par1World.setBlockMetadataWithNotify(x, y, z, 3, 2);
	    }
	    if (var6 == 3) {
	      par1World.setBlockMetadataWithNotify(x, y, z, 4, 2);
	    }
	  }
	  
	  public void registerIcons(IconRegister ir)
	  {
	    Icon[] icons = Util.registerSides(ir, this);
	    this.top = icons[0];
	    this.bottom = icons[1];
	    this.side = icons[2];
	    this.front = icons[3];
	  }
	  
	  private void setDefaultDirection(World world, int x, int y, int z)
	  {
	    if (!world.isRemote) {
	      world.setBlockMetadataWithNotify(x, y, z, 3, 2);
	    }
	  }
}
