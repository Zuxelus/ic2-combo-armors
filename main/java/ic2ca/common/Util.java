package ic2ca.common;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.util.Icon;

public class Util 
{
	public static final String MOD_ID = "IC2CA";
	public static final String MOD_NAME = "IndustrialCraft 2 Combo Armors";
	public static final String VERSION = "1.14.3.08";
	public static final String DEPENDENCIES = "required-after:IC2";
	public static final String CHANNEL = "ic2ca";

	public static String getFileName(Block block)
	{
		return block.getUnlocalizedName().substring(block.getUnlocalizedName().indexOf(".") + 1);
	}

	public static String getFileName(Item item)
	{
		return item.getUnlocalizedName().substring(item.getUnlocalizedName().indexOf(".") + 1);
	}

	public static Icon register(IconRegister ir, Block b)
	{
		return register(ir, getFileName(b));
	}

	public static Icon register(IconRegister ir, Item i)
	{
		return register(ir, getFileName(i));
	}

	public static Icon register(IconRegister ir, String s)
	{
		return ir.registerIcon(MOD_ID + ":" + s);
	}

	public static Icon[] registerSides(IconRegister ir, Block block)
	{
		Icon[] icon = new Icon[4];
		icon[0] = register(ir, getFileName(block) + "_top");
		icon[1] = register(ir, getFileName(block) + "_bottom");
		icon[2] = register(ir, getFileName(block) + "_side");
		icon[3] = register(ir, getFileName(block) + "_front");
		return icon;
	}
	
}
