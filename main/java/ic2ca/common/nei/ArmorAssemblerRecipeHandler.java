package ic2ca.common.nei;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.lwjgl.opengl.GL11;

import ic2.api.recipe.IRecipeInput;
import ic2.api.recipe.RecipeOutput;
import ic2.api.recipe.Recipes;
import ic2.core.util.StackUtil;
import ic2ca.api.IArmorAssemblerRecipeManager;
import ic2ca.common.ArmorAssemblerRecipes;
import ic2ca.common.gui.GuiArmorAssembler;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.item.ItemStack;
import codechicken.core.gui.GuiDraw;
import codechicken.nei.NEIServerUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.TemplateRecipeHandler;

public class ArmorAssemblerRecipeHandler extends TemplateRecipeHandler 
{
	int ticks;

	public class CachedArmorAssemblerRecipe extends TemplateRecipeHandler.CachedRecipe
	{
		public PositionedStack output;
		public List<PositionedStack> ingredients = new ArrayList();

		public CachedArmorAssemblerRecipe(IRecipeInput i1, IRecipeInput i2, RecipeOutput output1)
		{
			super();
			List<ItemStack> list1 = new ArrayList();
			List<ItemStack> list2 = new ArrayList();
			for (ItemStack item : i1.getInputs()) {
				list1.add(StackUtil.copyWithSize(item, i1.getAmount()));
			}
			for (ItemStack item : i2.getInputs()) {
				list2.add(StackUtil.copyWithSize(item, i2.getAmount()));
			}
			this.ingredients.add(new PositionedStack(list1, 63, 1));
			this.ingredients.add(new PositionedStack(list2, 37, 1));
			this.output = new PositionedStack(output1.items.get(0), 117, 19);
		}

		public List<PositionedStack> getIngredients()
		{
			return getCycledIngredients(ArmorAssemblerRecipeHandler.this.cycleticks / 20, this.ingredients);
		}

		public PositionedStack getResult()
		{
			return this.output;
		}
	}

	@Override
	public Class<? extends GuiContainer> getGuiClass()
	{
		return GuiArmorAssembler.class;
	}

	@Override
	public String getGuiTexture() {
		return "ic2ca:textures/gui/GuiArmorAssembler2.png";
	}

	public String getRecipeId()
	{
		return "ic2ca.armorassembler";
	}


	public Map<IArmorAssemblerRecipeManager.Input, RecipeOutput> getRecipeList()
	{
		return ArmorAssemblerRecipes.assembly().getRecipes();
	}	

	@Override
	public String getRecipeName() {
		return "Armor Assembler";
	}	

	@Override
	public void loadCraftingRecipes(ItemStack result)
	{
		for (Iterator i$ = getRecipeList().entrySet().iterator(); i$.hasNext();)
		{
			Map.Entry<IArmorAssemblerRecipeManager.Input, RecipeOutput> entry = (Map.Entry)i$.next();
			for (ItemStack output : ((RecipeOutput)entry.getValue()).items) {
				if (NEIServerUtils.areStacksSameTypeCrafting(output, result))
				{
					this.arecipes.add(new CachedArmorAssemblerRecipe(((IArmorAssemblerRecipeManager.Input)entry.getKey()).i1, ((IArmorAssemblerRecipeManager.Input)entry.getKey()).i2, (RecipeOutput)entry.getValue()));
					break;
				}
			}
		}
	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if (outputId.equals(getRecipeId())) {
			for (Map.Entry<IArmorAssemblerRecipeManager.Input, RecipeOutput> entry : getRecipeList().entrySet()) {
				this.arecipes.add(new CachedArmorAssemblerRecipe(((IArmorAssemblerRecipeManager.Input)entry.getKey()).i1, ((IArmorAssemblerRecipeManager.Input)entry.getKey()).i2, (RecipeOutput)entry.getValue()));
			}
		} else {
			super.loadCraftingRecipes(outputId, results);
		}
	}

	@Override
	public void loadUsageRecipes(ItemStack ingredient)
	{
		for (Map.Entry<IArmorAssemblerRecipeManager.Input, RecipeOutput> entry : getRecipeList().entrySet()) {
			if ((((IArmorAssemblerRecipeManager.Input)entry.getKey()).i1.matches(ingredient)) || (((IArmorAssemblerRecipeManager.Input)entry.getKey()).i2.matches(ingredient))) {
				this.arecipes.add(new CachedArmorAssemblerRecipe(((IArmorAssemblerRecipeManager.Input)entry.getKey()).i1, ((IArmorAssemblerRecipeManager.Input)entry.getKey()).i2, (RecipeOutput)entry.getValue()));
			}
		}
	}

	@Override
	public void drawBackground(int i)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GuiDraw.changeTexture(getGuiTexture());
		GuiDraw.drawTexturedModalRect(0, 0, 5, 16, 140, 65);
	}

	@Override
	public void drawExtras(int i)
	{
		float f = this.ticks >= 20 ? (this.ticks - 20) % 20 / 20.0F : 0.0F;
		drawProgressBar(74, 19, 176, 14, 25, 16, f, 0);
		f = this.ticks <= 20 ? this.ticks / 20.0F : 1.0F;
		drawProgressBar(51, 20, 176, 0, 14, 14, f, 3);
	}

	@Override
	public void onUpdate()
	{
		super.onUpdate();
		this.ticks += 1;
	}
}
